#!/usr/bin/python

import os.path
import os
import sys
import re
import subprocess
import datetime
import time
import config

def check_ts3500_vol(vol):
    
    vol_list = get_libvol('TS3500')
    on_site = False 

    if (vol in vol_list):
        on_site =  True

    return on_site


def get_filepath_by_vol(vol_file):
    node_name = [
     'PTSTV',
     'HAKKATV',
     'MVTV',
     'TITV',
     'NEWSPTS',
     'NEWSHAKKA',
     'NEWSTITV'
    ]
    root = ''
    fh = open(vol_file, 'r')
    lines = fh.readlines()
    path_list = []
    path_head = ''
    path_trail = ''
    root_path = ''
    node = ''
    for l in lines:
        l = l.rstrip()
        list = re.split('\s+', l)
        #print(l)
        #print(list)
        if (len(path_head) > 0):
            if (node == 'NEWSHAKKA'):
                path_trail = list[2]
            else:
                path_trail = list[1]
            full_path = root_path + path_head + path_trail
            #print(full_path)
            path_list.append(full_path)

        node = list[0]
        path_head = ''
        path_trail = ''
        if (node == 'PTSTV'):
            root_path = '/ptstv'
            path_head = list[4].rstrip('-')
        elif (node == 'HAKKATV'):
            root_path = '/hakkatv'
            path_head = list[4].rstrip('-')
        elif (node == 'MVTV'):
            root_path = '/mvtv'
            path_head = list[4].rstrip('-')
        elif (node == 'NEWSPTS'):
            root_path = '/news/pts'
            path_head = list[4].rstrip('-')
        elif (node == 'NEWSHAKKA'):
            root_path = '/news/hakka'
            path_head = list[4].rstrip('-')
        elif (node == 'NEWSTITV'):
            root_path = '/news/titv'
            path_head = list[4].rstrip('-')
        else:
            begin_path = 0
            path_head = ''
            #Unknow node

    return path_list


def get_vol_status_by_stgpool(stg_pool):
    VOL_DIR = '/mnt/GPFS/VOL/'
    VOL_STATUS_DIR = '/mnt/GPFS/VOL_STATUS/'
    STGPOOL_DIR = '/mnt/GPFS/STGPOOL_VOL/'
    login = config.TSM_CONFIG.tsm_user1[0]
    passwd = config.TSM_CONFIG.tsm_user1[1]
    dsmadmc = 'dsmadmc -id=' + login + ' -password=' + passwd + ' '
    on_site = False
    stgpool_file = STGPOOL_DIR + stg_pool
    status_file_name  = VOL_STATUS_DIR + stg_pool
    f_status = open(status_file_name, 'w')

    vol_list = []
    vol_list = get_vol_name(stgpool_file)
    tmp_file = "vol_status.tmp"
    reObj = re.compile(r'[A-Z]\d{5}L4')
    for vol in vol_list:
        on_site = check_ts3500_vol(vol)
        query_cmd = 'query volume ' + vol
        cmd = dsmadmc + query_cmd + ' > ' + tmp_file
        os.system(cmd)
        fh = open(tmp_file, 'r')
        lines = fh.readlines()
        need_update_list = False
        for l in lines:
            l = l.rstrip()
            list = re.split("\s+", l)
            #print(list[0])
            if (len(list) > 1):
                res = reObj.match(list[0])
                if (res):
                    stgpool = list[1]
                    capacity = list[3] + list[4]
                    vol_status = list[6]
                    if (on_site == True):
                        on_site_info = "on"
                    else:
                        on_site_info = "off"
                    vol_status = vol + '_' + stgpool + '_'+ vol_status + '_' + capacity + '_' + on_site_info
                    print(vol_status) #NEWSTITVHSM_E01627L4_Filling_1.5T
                    f_status.writelines(vol_status + '\n')
                    vol_file = VOL_DIR + '/' + vol
                    vol_file_path = VOL_DIR + '/' + vol_status
                    if (os.path.isfile(vol_file_path)):
                        if (vol_status == 'Filling'):
                            need_update_list = True
                    else: #need create new file
                        need_update_list = True

                    if (need_update_list == True):        
                        fh_vol_file_path = open(vol_file_path, "w")
                        path_list = get_filepath_by_vol(vol_file)
                        for p in path_list:
                            print (vol + ":" + p)
                            fh_vol_file_path.writelines(p + '\n')


        #print(vol)


    #query volume A00008L4 :
    #Storage pool name : NEWSPTSHSM
    #Estimated Capacity : 875.1G
    # Volume Status : Full
    # Todo : rename existing vvolume file suffixed with storage pool name 
    # Extract E00974L4, get file list and save in NEWSPTSHSM_E00974L4_FULL.txt



def get_file_size(path):
    size = 0
    if (os.path.isfile(path)):
        statInfo = os.stat(path)
        size = statInfo.st_size

    return size

def get_gpfs_size(path):
    if (not os.path.isfile(path)):
        print("Error : " + path + " not found!")
        return False

    proc = subprocess.Popen(['dsmls', path], stdout=subprocess.PIPE)
    res = proc.stdout.readlines()
    tmp_arr = res[8].split()
    actuall_size = tmp_arr[0]
    resident_size = tmp_arr[1]
    res = [actuall_size, resident_size]

    return res

def tolinux_path(win_path):
    item = []
    win_path = win_path.rstrip()
    item = win_path.split('\\')
    #print item
    linux_path = ''
    for i in range(4, len(item), 1):
        #print item[i]
        linux_path += '/' + item[i]

    return linux_path


def doInDir(someDir):
    print (someDir)
    fileList = os.listdir(someDir)
    for f in fileList:
        fullPath = os.path.join(someDir, f)
        if os.path.isdir(fullPath):
           doInDir(fullPath)
        elif os.path.isfile(fullPath):
           print(fullPath)

def touch_file(infile, target_dir):
    fh = open(infile, "r")
    lines = fh.readlines()
    for l in lines:
        videoid = l.rstrip()
        if (videoid == "NULL" or videoid == ''):
            continue

        vf = target_dir +  "/" + videoid + ".mxf"
        cmd = "touch " +  vf
        print(cmd)
        os.system(cmd)

#get all lib volume in tape library
#query libvol TS3500
def get_libvol(lib_name):
    #lib_name = TS3500, TS3310
    vol_list = []
    time_stamp = datetime.datetime.now()
    date_time = time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime())
    outf = lib_name + "-" + date_time + ".txt"
    login = config.TSM_CONFIG.tsm_user1[0]
    passwd = config.TSM_CONFIG.tsm_user1[1]

    command = 'dsmadmc -id=' + login + ' -password='  + passwd + ' query libvol ' + lib_name + ' >' + outf
    os.system(command)

    vol_list = get_libvol_name(outf)
    """
    for vol in vol_list:
        print vol
    """

    return vol_list

#get unavailable volumes
def get_unavailable_vol():
    vol_list = []
    time_stamp = datetime.datetime.now()
    date_time = time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime())
    outf = "unavail-" + date_time + ".txt"
    login = config.TSM_CONFIG.tsm_user1[0]
    passwd = config.TSM_CONFIG.tsm_user1[1]

    command = 'dsmadmc -id=' + login + ' -password='  + passwd + ' query vol access=unavailable  >' + outf
    os.system(command)

    vol_list = get_vol_name_from_unavail(outf)
    """
    for vol in vol_list:
        print vol
    """

    return vol_list

#extract vol name from file unavail-xxx.txt(output of q vol acc=unavail)
def get_vol_name_from_unavail(infile):
    fh = open(infile, "r")
    lines = fh.readlines()
    count = 1
    vol_list = []

    for l in lines:
        l = l.rstrip()
        list = re.split("\s+", l)
        #print(list)
        reObj = re.compile(r'[A-Z]{1}\d{5}L4')
        if (len(list) > 1):
            res = reObj.match(list[0])
            if (res):
                vol_list.append(res.group(0))
                count = count + 1

    return vol_list

#extract vol name from file TS3500.txt(output of q libvol TS3500)
def get_libvol_name(infile):
    fh = open(infile, "r")
    lines = fh.readlines()
    count = 1
    vol_list = []

    for l in lines:
        l = l.rstrip()
        list = re.split("\s+", l)
        #print(list)
        reObj = re.compile(r'[A-Z]{1}\d{5}L4')
        if (len(list) > 1):
            res = reObj.match(list[1])
            if (res):
                vol_list.append(res.group(0))
                count = count + 1

    return vol_list

#extract vol name from file NEWSHAKKAHSM.txt
#query vol STGpool=NEWSHAKKAHSM
def get_vol_name(infile):
    fh = open(infile, "r")
    lines = fh.readlines()
    count = 1
    vol_list = []

    for l in lines:
        l = l.rstrip()
        list = re.split("\s+", l)
        #print(list)
        reObj = re.compile(r'[A-Z]{1}\d{5}L4')
        #reObj1 = re.compile(r'([A-Z]{1}\d{5}L4).*(Full|Filling)')
        reObj1 = re.compile(r'Full|Filling')
        res = reObj.match(list[0])
        if (res):
            vol_name = res.group(0)
            if (len(list) == 7):
                if (list[6] == 'Full'): #only dump full volume
                    vol_list.append(vol_name)
                    count = count + 1

    return vol_list

#stgpool  = NEWSHAKKAHSM
def get_offsite_vol(stgpool):
    stgpoolf = stgpool
    login = config.TSM_CONFIG.tsm_user1[0]
    passwd = config.TSM_CONFIG.tsm_user1[1]
    dsmadmc = 'dsmadmc -id=' + login + ' -password=' + passwd
    query_cmd = dsmadmc + " " + "query vol STGPool=" + stgpool + " >" + stgpoolf
    os.system(query_cmd) #get all vol of STGPool
    #extract vol name from file
    stgpool_vol_list = get_vol_name(stgpoolf)
    libvol_list = get_libvol("TS3500")
    for vol in stgpool_vol_list:
        if (vol not in libvol_list):
            print(vol)

#def get_vol_content(stgpool):

def get_content_by_vol(vol):
    #extract volume content
    TARGET_DIR = config.MAM_STORAGE.tsm_vol
    user = config.TSM_CONFIG.tsm_user1[0]
    password = config.TSM_CONFIG.tsm_user1[1]
    dsmconn = 'dsmadmc -id=' + user +  ' -password=' + password + ' '

    count = 1
    outf = TARGET_DIR + vol

    print(str(count) + " Get volume " + vol)
    dsmcommand = dsmconn + 'query content ' + vol + '>' + outf
    os.system(dsmcommand)
    
