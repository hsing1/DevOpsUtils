#!/usr/bin/python
#-*- coding: UTF-8 -*-
import os
import sys

#TARGET_ROOT = "\\\\10.13.200.97\\mam_data\\200_15\\Macroview\\2016"
#TARGET_ROOT = "\\\\10.13.200.97\\mam_data\\200_15\\Macroview\\2017"
#TARGET_ROOT = "\\\\10.13.200.97\\mam_data\\200_15\\Macroview\\2018"
#TARGET_ROOT = "\\\\10.13.200.97\\mam_data\\200_15\\HakkaMgz"
#TARGET_ROOT = "/mnt/nas97/mam_data/200_15_Macroview/2017"
OUTPUT_DIR = sys.argv[1] 

def dump_file_list(target_dir, out_file):
    count = 0
    total_size = 0
    fh = open(out_file, "w")
    print("Scan " + target_dir + " ...")
    for r, ds, fs in os.walk(target_root):
        #print ("root = " + r)
        #print ("loop files")
        for f in fs:
            path = os.path.join(r, f)
            fh.writelines(path + "\n")
            stat_info = os.stat(path)
            total_size += stat_info.st_size
            count += 1

        #print ("loop dirs")
        #for d in ds:
        #    print ("DIR = " + os.path.join(r, d))

    total_size = total_size/1024/1024/1024
    print("Total files=" + str(count) + ", Total file size = " + str(total_size) + " GB")
    fh.close()
    return count

def test_case(target_dir):
    abspath = os.path.abspath(target_root)
    print(abspath)
    path_info = os.path.dirname(abspath)
    print(path_info)
    path_list= str.split(abspath,  '/')
    print(path_list)
    channel = path_list[2]
    year = path_list[3]
    out_file = channel + "_" + year + ".txt"
    print(out_file)


TARGET_ROOT_LIST = ["/news/titv/2011",
    "/news/titv/2012",
    "/news/titv/2013",
    "/news/titv/2015",
    "/news/titv/2016",
    "/news/titv/2017",
    ]

TARGET_ROOT_LIST = ["/news/titv/2010"]

total_count = 0
for target_root in TARGET_ROOT_LIST:
    #print(TARGET_ROOT)
    #test_case(target_root)
    abspath = os.path.abspath(target_root)
    path_list = str.split(abspath, '/')
    list_type = path_list[1]
    channel = path_list[2]
    year = path_list[3]
    out_file = OUTPUT_DIR + "/" + list_type + "_" + channel + "_" + year + ".txt"

    total_count += dump_file_list(target_root, out_file)

print("Total files = " + str(total_count))

