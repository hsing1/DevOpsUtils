#!/usr/bin/python

import sys
import os
import ptsutils3
import config
import re
import shutil

#To dump content of volume by traverse volume list in each STGpool 
#Usage : get_content_by_stgpool.py PTSTVHSM|HAKKATVHSM

INPUT_DIR = "\\\\isilon.pts.org.tw\\MAM_ARCHIVE\\TSM_INFO\\STGPOOL_VOL"
#INPUT_DIR = "\\\\10.13.200.25\\test\\GPFS\\STGPOOL_VOL"
TARGET_DIR = '\\\\isilon.pts.org.tw\\MAM_ARCHIVE\\TSM_INFO\VOL'
#TARGET_DIR = "\\\\10.13.200.25\\test\\GPFS\\VOL"
POOL_LIST = [
    'PTSTVHSM',
    'HAKKATVHSM',
    'TITVHSM',
    'MVTVHSM',
    'NEWSHAKKAHSM',
    'NEWSPTSHSM',
    'NEWSTITVHSM',
    'PTSTVHSMCP'
]

hsm_login = config.HSM_CONFIG.login
hsm_passwd =  config.HSM_CONFIG.password
tsm_login = config.TSM_CONFIG.tsm_user1[0] 
tsm_passwd = config.TSM_CONFIG.tsm_user1[1]
hsm = config.HSM_CONFIG.HSM['HSM4']

plink_args = 'plink -ssh {hsm} -l {login} -pw {passwd}'.format(hsm = hsm, login = hsm_login, passwd = hsm_passwd)
dsmadmc_args = 'dsmadmc -id={login} -password={passwd}'.format(login = tsm_login, passwd = tsm_passwd)
connection = '{plink} {dsmadmc}'.format(plink = plink_args, dsmadmc = dsmadmc_args)

user = config.TSM_CONFIG.tsm_user1[0]
password = config.TSM_CONFIG.tsm_user1[1]
dsmconn = 'dsmadmc -id=' + user +  ' -password=' + password + ' '

def check_file(vol, outf):
    size = 0
    while (True):
        size = ptsutils3.get_file_size(outf)
        #print(msg)
        if (size < 1000): # regenerate file
            msg = "uncomplete file, regenerate vol {v}".format(v = vol)
            print(msg)
            ptsutils3.dump_vol_content_info(vol, outf)
        else:
            break
    return size

def get_content(vol_list, out_dir):
    HSM = False
    HSMCP = False
    BAK = False
    BAKCP = False
    #parsing PTSTVHSM.txt to obtain volume list
    re_hsm = re.compile(r'.*HSM$')
    re_hsmcp = re.compile(r'.*HSMCP$')
    re_bak = re.compile(r'.*BAK$')
    re_bakcp = re.compile(r'.*BAKCP$')
    if (re_hsm.match(out_dir)):
        HSM = True
    if (re_hsmcp.match(out_dir)):
        HSMCP = True
    if (re_bak.match(out_dir)):
        BAK= True
    if (re_bakcp.match(out_dir)):
        BAKCP= True
    if (not os.path.isdir(out_dir)):
        os.makedirs(out_dir)

    count = 1
    for vol in vol_list:
        if (not os.path.isdir(out_dir)):
            os.makedirs(out_dir)

        outf = os.path.join(out_dir, vol)
        if (os.path.isfile(outf)): #already dumped
            #print(outf + " already exists")
            size = check_file(vol, outf)
        else:
            ptsutils3.dump_vol_content_info(vol, outf)
            size = check_file(vol, outf)
            msg = '{c} Get volume content: {v} ({s})'.format(c = str(count), v = vol, s = str(size))
            print(msg)

        #copy file to HSM folder
        if (HSM):
            hsm_dir = os.path.join(TARGET_DIR, 'HSM')
            if (not os.path.isdir(hsm_dir)):
                os.makedirs(hsm_dir)

            hsmf = os.path.join(hsm_dir, vol)
            
            size1 = ptsutils3.get_file_size(outf)
            size2 = ptsutils3.get_file_size(hsmf)

            if (size1 != size2):
                shutil.copy2(outf, hsmf)

        if (HSMCP):
            hsm_dir = os.path.join(TARGET_DIR, 'HSMCP')
            if (not os.path.isdir(hsm_dir)):
                os.makedirs(hsm_dir)

            hsmcpf = os.path.join(hsm_dir, vol)
            if (not os.path.isfile(hsmcpf)):
                shutil.copy2(outf, hsmcpf)
        if (BAK):
            bak_dir = os.path.join(TARGET_DIR, 'BAK')
            if (not os.path.isdir(bak_dir)):
                os.makedirs(bak_dir)

            bakf = os.path.join(bak_dir, vol)
            if (not os.path.isfile(bakf)):
                shutil.copy2(outf, bakf)
        if (BAKCP):
            bak_dir = os.path.join(TARGET_DIR, 'BAKCP')
            if (not os.path.isdir(bak_dir)):
                os.makedirs(bak_dir)

            bakcpf = os.path.join(bak_dir, vol)
            if (not os.path.isfile(bakcpf)):
                shutil.copy2(outf, bakcpf)


        #if out_dir is HSM, copy file to HSM folder
        #if out_dir is HSMCP, copy file to HSMCP folder
        #if out_dir is BAK, copy file to BAK folder
        #if out_dir is BAKCP, copy file to BAMCP folder
        count = count + 1

def get_content_org2(vol_list, out_dir):
    HSM = False
    HSMCP = False
    BAK = False
    BAKCP = False
    #parsing PTSTVHSM.txt to obtain volume list
    re_hsm = re.compile(r'.*HSM$')
    re_hsmcp = re.compile(r'.*HSMCP$')
    re_bak = re.compile(r'.*BAK$')
    re_bakcp = re.compile(r'.*BAKCP$')
    if (re_hsm.match(out_dir)):
        HSM = True
    if (re_hsmcp.match(out_dir)):
        HSMCP = True
    if (re_bak.match(out_dir)):
        BAK= True
    if (re_bakcp.match(out_dir)):
        BAKCP= True
    if (not os.path.isdir(out_dir)):
        os.makedirs(out_dir)


    count = 1
    for vol in vol_list:
        outf = os.path.join(out_dir, vol)
        if (os.path.isfile(outf)): #already dumped
            #print(outf + " already exists")
            continue
        print(str(count) + " " + "Get volume content:" + outf)
        #print(" Get volume " + vol)
        dsmcommand = dsmconn + 'query content ' + vol + '>' + outf
        #print(dsmcommand)
        os.system(dsmcommand)
        #copy file to HSM folder
        if (HSM):
            hsmf = os.path.join(TARGET_DIR, 'HSM/', vol)
            if (not os.path.isfile(hsmf)):
                shutil.copy2(outf, hsmf)
        if (HSMCP):
            hsmcpf = os.path.join(TARGET_DIR, 'HSMCP/', vol)
            if (not os.path.isfile(hsmcpf)):
                shutil.copy2(outf, hsmcpf)
        if (BAK):
            bakf = os.path.join(TARGET_DIR, 'BAK/', vol)
            if (not os.path.isfile(bakf)):
                shutil.copy2(outf, bakf)
        if (BAKCP):
            bakcpf = os.path.join(TARGET_DIR, 'BAKCP/', vol)
            if (not os.path.isfile(bakcpf)):
                shutil.copy2(outf, bakcpf)


        #if out_dir is HSM, copy file to HSM folder
        #if out_dir is HSMCP, copy file to HSMCP folder
        #if out_dir is BAK, copy file to BAK folder
        #if out_dir is BAKCP, copy file to BAMCP folder
        count = count + 1

def get_content_org(vol_list):
    #parsing PTSTVHSM.txt to obtain volume list
    count = 1
    for vol in vol_list:
        outf = TARGET_DIR + vol
        if (os.path.isfile(outf)): #already dumped
            #print(outf + " already exists")
            continue

        print(str(count) + " Get volume " + vol)
        #print(" Get volume " + vol)
        dsmcommand = dsmconn + 'query content ' + vol + '>' + outf
        #print(dsmcommand)
        os.system(dsmcommand)
        count = count + 1

def main(argv):
    DUMP_TYPE = 1 #Only dump Full volume
    if (len(sys.argv) == 3):
        infile = INPUT_DIR + sys.argv[1]
        out_dir = sys.argv[2]
        if (os.path.isfile(infile)):
            #extract volume list from stgpool
            vol_list  = ptsutils3.get_vol_name(infile, DUMP_TYPE)
            #create G00016L4 file
            get_content(vol_list, out_dir)
        else:
            print("Cannot find " + infile)
    else:
        for pool in POOL_LIST:
            infile = os.path.join(INPUT_DIR, pool + '.txt')
            out_dir = os.path.join(TARGET_DIR, pool)
            vol_list  = ptsutils3.get_vol_name(infile, DUMP_TYPE)
            get_content(vol_list, out_dir)

"""
def main_org(argv):
    DUMP_TYPE = 1 #Only dump Full volume
    if (len(sys.argv) == 3):
        infile = INPUT_DIR + sys.argv[1]
        out_dir = sys.argv[2]
        if (os.path.isfile(infile)):
            #extract volume list from stgpool
            vol_list  = ptsutils.get_vol_name(infile, DUMP_TYPE)
            #create G00016L4 file
            get_content(vol_list, out_dir)
        else:
            print("Cannot find " + infile)
    else:
        for pool in POOL_LIST:
            infile = INPUT_DIR + pool
            out_dir = TARGET_DIR + pool
            vol_list  = ptsutils.get_vol_name(infile, DUMP_TYPE)
            get_content(vol_list, out_dir)

def main_old(argv):
    DUMP_TYPE = 1 #Only dump Full volume
    if (len(sys.argv) == 2):
        infile = INPUT_DIR + sys.argv[1]
        if (os.path.isfile(infile)):
            #extract volume list from stgpool
            vol_list  = ptsutils.get_vol_name(infile, DUMP_TYPE)
            #create G00016L4 file
            get_content(vol_list)
        else:
            print("Cannot find " + infile)
    else:
        for pool in POOL_LIST:
            infile = INPUT_DIR + pool
            vol_list  = ptsutils.get_vol_name(infile, DUMP_TYPE)
            get_content(vol_list)
"""


if (__name__ == "__main__"):
    main(sys.argv[1:])