#!/usr/bin/python
#-*- coding: UTF-8 -*-
import re
import os
import sys
#import ptsutils3 no pyodbc on MAC

infile = sys.argv[1]
outfile = "2app.txt"
file_offsite = "offsite.txt"

fh = open(infile, "r", encoding = "utf-8")
ofh = open(outfile, "w")
fh_offsite = open(file_offsite, "w")

def my_regex(input, pattern):
    reObj = re.compile(pattern)
    res = reObj.match(input)
    return res

lines = fh.readlines()
video_id_pat = r'.+?(00\w{4})\.mxf'
file_no_pat = r'.+?([GP]\d{14,15})\.mxf'
#file_no_pat = r'[]+?([GP]\d{14,15})\.mxf'
pattern1 = r'.+TSM-ReCall.+'
pattern2 = r'磁帶不在架上.+'  #remove ^ because of \ufeff in the first line
pattern3 = r'^正在上傳.+'
pattern4 = r'^可以開始複製.+'
unc_path = ""
video_id = ""
file_no = ""
status = ""
source_file = ""
target_file = ""

for l in lines:
    status = "Tape on-site"
    l = l.rstrip()
    if (len(l) < 50):
        continue
    print("Extract : " + l)
    fields = l.split('\t')
    source_file = fields[5]
    target_file = fields[6]

    """
    res = my_regex(l, unc_path_pat)
    if (res != None):
        unc_path = res.group(0)
        print("UNC:" + unc_path)
    """

    #print("    get video id :")
    res = my_regex(l, video_id_pat)
    if (res != None):
        #target_file = res.group(0)
        video_id = res.group(1)
    else:
        print("Error: cannot extract video id")

    #print("    get file number :")
    res = my_regex(l, file_no_pat)
    if (res != None):
        #source_file = res.group(0)
        file_no = res.group(1)
    else:
        print("Error: cannot extract file no")

    res = my_regex(l, pattern1)
    if (res != None):
        status = "Need Copy"

    res = my_regex(l, pattern4)
    if (res != None):
        status = "Need Copy"
    
    #check tape offsite
    res = my_regex(l, pattern2)
    if (res != None):
        status = "Tape offsite"
        print(source_file)
        fh_offsite.write(source_file + "\n")
        print(video_id + "-" + file_no + ":" + status)
        print("")
        print("")
        continue

    res = my_regex(l, pattern3)
    if (res != None):
        status = "Uploading ..."
        print(video_id + "-" + file_no + ":" + status)
        print("")
        print("")
        continue

    if (status == "Need Copy"):
        out_str = video_id + " " + source_file + "\n"
        ofh.write(out_str)
        print("copy file " + source_file + " to " + video_id)

    print("")
    print("")
