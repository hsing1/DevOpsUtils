import sys
import os
import pyodbc
import config

infile = sys.argv[1] #FSFILE_NO list
fh = open(infile, "w")

VOL_DIR = '\\10.13.200.30\GPFS\VOL'

server = config.MAM_CONFIG.db_server
database = config.MAM_CONFIG.database
username = config.MAM_CONFIG.db_user2[0]
password = config.MAM_CONFIG.db_user2[1]
conn_str = 'DRIVER={{ODBC Driver 13 for SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
cnxn = pyodbc.connect(conn_str)
cursor = cnxn.cursor()

tsql = ("select FSFILE_NO, FSID, FNEPISODE, FSARC_TYPE, FCFILE_STATUS, FSFILE_PATH_H, FSJOB_ID from TBLOG_VIDEO where FSVIDEO_PROG = '00004TF6';")
with cursor.execute(tsql):
    row = cursor.fetchone()
    while row:
        fsfile_no = str(row[0])
        fsid = str(row[1])
        fnepisode = str(row[2])
        fsarc_type = str(row[3])
        fsfile_status = str(row[4])
        fsfile_path = str(row[5])
        fsjob_id = str(row[6])
        
        
        info = fsfile_no + ":" + fsid + ":" + fnepisode + ":" + fsarc_type + ":" + fsfile_status
        print(info)
        print("FILE PATH : " + fsfile_path)
        row = cursor.fetchone()

cnxn.close()