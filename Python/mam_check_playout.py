# -*- coding: UTF-8 -*-

import os
import sys
import getopt

HDUPLOAD_DIR = '\\\\10.13.200.25\\HDUpload'
REVIEW_DIR = '\\\\10.13.200.22\\mamnas1\\Review'
APPROVED_DIR = '\\\\10.13.200.22\\mamnas1\\Approved'
HAKKANAS = '\\\\10.13.32.230\\hakkaonair'


def check_file(target):
    if (os.path.isfile(target)):
        print("%s exists" %target)
    else:
        print("%s doesn't exists" %target)


def check_all_path(video_id):
    fname = video_id + '.mxf'
    target = os.path.join(HAKKANAS, fname)
    check_file(target)
    target = os.path.join(HDUPLOAD_DIR, fname)
    check_file(target)
    target = os.path.join(REVIEW_DIR, fname)
    check_file(target)
    target = os.path.join(APPROVED_DIR, fname)
    check_file(target)

def check_by_file(flist):
    fh = open(flist, "r")
    lines = fh.readlines()
    for l in lines:
        vid = l.rstrip()
        if (len(vid) == 8):
            vid = vid[2:]
        check_all_path(vid)
        print("\n")

def check_by_file_approved(flist):
    fh = open(flist, "r")
    lines = fh.readlines()
    for l in lines:
        vid = l.rstrip()
        if (len(vid) == 8):
            mxf = vid[2:] + '.mxf'
        else:
            mxf = vid + '.mxf'

        target = os.path.join(APPROVED_DIR, mxf)
        if (not os.path.isfile(target)):
            print("%s doesn't exist!" %target)

def main(argv):
    DO_CHECK_BY_FILE = False
    DO_CHECK_BY_FILE_ON_APPROVED = False
    DO_CHECK_ALL_PATH = False

    try:
        opts, args = getopt.getopt(argv, "a:h:r:", ["all=", "vlist=", "vlist_approved="])

    except Exception as e:
        template = "Exception {0} occure : {1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)

    for opt, arg in opts:
        print("Process opt %s, %s" %(opt, arg))
        if opt in ('-h'): #check HDUpload
            fname = arg + '.mxf'
            target = os.path.join(HDUPLOAD_DIR, fname)
            check_file(target)
        elif opt in ('-r'): #check Review
            fname = arg + '.mxf'
            target = os.path.join(REVIEW_DIR, fname)
            check_file(target)
        elif opt in ('-a'): #check Approve
            fname = arg + '.mxf'
            target = os.path.join(APPROVED_DIR, fname)
            check_file(target)
        elif opt in ('--all'):
            DO_CHECK_ALL_PATH = True
            vid = arg
            check_all_path(vid)
        elif opt in ("--vlist"):
            flist = arg
            DO_CHECK_BY_FILE = True
        elif opt in ("--vlist_approved"):
            flist_app = arg
            DO_CHECK_BY_FILE_ON_APPROVED = True


    
    if (DO_CHECK_BY_FILE):
        check_by_file(flist)
    elif (DO_CHECK_ALL_PATH):
        check_all_path(vid)
    elif (DO_CHECK_BY_FILE_ON_APPROVED):
        check_by_file_approved(flist_app)



if (__name__ == "__main__"):
    main(sys.argv[1:])