import sys
import os
import getopt
import config
import pyodbc
from datetime import datetime, timedelta


# Ref E:\PTSMAM_Source\PTS_MAM\PTS_MAM3.Web\DataSource\TransProgList.asmx.cs 
# ExportModProglist()
# channel = PTS_MOD. DIMO, HD_MOD, PSHOW_HD
def export_mod_prog_list(channel):
    print("DO export_mod_prog_list")
    server = config.MAM_CONFIG.db_server
    database = config.MAM_CONFIG.database
    username = config.MAM_CONFIG.db_user1[0]
    password = config.MAM_CONFIG.db_user1[1]
    connStr = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)

    cnxn1 = pyodbc.connect(connStr)
    cursor1 = cnxn1.cursor()

    #get table name for specified channel
    fsprogramx_table_name = ''
    fsparea_cheid = ''
    fsoutput_file_name = ''
    tsql = "exec SP_Q_TBPGM_PROG_EPG ?"
    with cursor1.execute(tsql, channel):
        row = cursor1.fetchone()
        while row:
            fsprogramx_table_name = str(row[2])
            fsparea_cheid = str(row[3])
            fsoutput_file_name = str(row[4])
            row = cursor1.fetchone()

    cnxn1.close()


    cnxn2 = pyodbc.connect(connStr)
    cursor2 = cnxn2.cursor()

    """
    tsql_q_playlist = "select A.PAENO FSPROGID,A.PSDAT FDDATE,A.PSTME FSBTIME,B.FSWEBNAME,A.PBENO FNEPISODE,B.FNTOTEPISODE"
    tsql_q_playlist += " from OPENQUERY(PTS_PROG_MYSQL, 'SELECT * FROM programX.{pshow}  where PSDAT>=sysdate() and PSDAT<= sysdate() + interval 14 day') A, TBPROG_M B"
    tsql_q_playlist += " where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME"
    """

    tsql_q_playlist = "select A.PAENO FSPROGID, A.PSDAT FDDATE, A.PSTME FSBTIME, B.FSWEBNAME,A.PBENO FNEPISODE, B.FNTOTEPISODE, "
    tsql_q_playlist += "ISNULL(D.FSPROGGRADENAME, '普') FSPROGGRADENAME,"
    tsql_q_playlist += "(CASE WHEN (C.FSPROGLANGID1 = C.FSPROGLANGID2) THEN 'N' ELSE 'Y' END) as FSDUAL_LANG"
    tsql_q_playlist += " from OPENQUERY(PTS_PROG_MYSQL, 'SELECT  * FROM programX.{pshow} where PSDAT>=sysdate() and PSDAT<= sysdate()+ interval 45 day') A "
    tsql_q_playlist += " join TBPROG_M B on A.PAENO = B.FSPROG_ID"
    tsql_q_playlist += " left join TBPROG_D C on C.FSPROG_ID = A.PAENO and A.PBENO = C.FNEPISODE"
    tsql_q_playlist += " left join TBZPROGGRADE D on C.FSPROGGRADEID = D.FSPROGGRADEID"
    tsql_q_playlist += " where A.PAENO=B.FSPROG_ID order by A.PSDAT,A.PSTME"

    tsql_q_playlist = tsql_q_playlist.format(pshow = fsprogramx_table_name)

    playlist = []
    prog_info = []
    with cursor2.execute(tsql_q_playlist):
        row = cursor2.fetchone()
        while row:
            #print(row)
            episode = int(row[4])
            total_episode = int(row[5])
            prog_name = str(row[3])

            if (total_episode == 1 or episode == 0): # non series
                fsname = prog_name #program name
            else:
                fsname = "{p}({e})".format(p = prog_name, e = str(episode))

            fsbdate = str(row[1])
            fsedate = ""
            s = str(row[2]) #23:00
            fsbtime = s[0:2] + s[3:5] #2300
            fsetime = ""

            bweek = ""
            fsprograde_id = str(row[6]).split('：')[0] #全形 ctrl + :
            fsdual_lang = str(row[7])
            filsname = ''
            bweek = ''
            prog_info = [fsname, fsbdate, fsedate, fsbtime, fsetime, filsname, bweek, fsprograde_id, fsdual_lang]
            playlist.append(prog_info)
            row = cursor2.fetchone()

    cnxn2.close()

    #calculate end_time, and convert play time greater than 24:00
    row_count = len(playlist)
    for i in range(row_count):
        if (i + 1 < row_count):
            next_begin_time = playlist[i + 1][3]
            playlist[i][4] = next_begin_time
            next_begin_date = playlist[i + 1][1]
            playlist[i][2] = next_begin_date

            begin_time = playlist[i][3]
            i_begin_time = int(begin_time)
            if (i_begin_time >= 2400):
                i_begin_time -= 2400
                playlist[i][3] = str(i_begin_time).zfill(4)
                begin_date = playlist[i][1]
                begin_date_obj = datetime.strptime(begin_date, '%Y-%m-%d')
                begin_date_obj += timedelta(days = 1) # add 1 day
                begin_date = datetime.strftime(begin_date_obj, '%Y-%m-%d')
                playlist[i][1] = begin_date

            end_time = playlist[i][4]
            i_end_time = int(end_time)
            if (i_end_time >= 2400):
                i_end_time -= 2400
                playlist[i][4] = str(i_end_time).zfill(4)
                end_date = playlist[i][2]
                end_date_obj = datetime.strptime(end_date, '%Y-%m-%d')
                end_date_obj += timedelta(days = 1) # add 1 day
                end_date = datetime.strftime(end_date_obj, '%Y-%m-%d')
                playlist[i][2] = end_date

            day_of_week = datetime.strptime(playlist[i][1], '%Y-%m-%d').weekday()
            if (day_of_week == 0): #Sunday
                playlist[i][6] = 'PRCB0'
            elif (day_of_week == 1): #Monday
                playlist[i][6] = 'PRCB1'
            elif (day_of_week == 2):
                playlist[i][6] = 'PRCB2'
            elif (day_of_week == 3):
                playlist[i][6] = 'PRCB3'
            elif (day_of_week == 4):
                playlist[i][6] = 'PRCB4'
            elif (day_of_week == 5):
                playlist[i][6] = 'PRCB5'
            elif (day_of_week == 6):
                playlist[i][6] = 'PRCB6'

            cnxn3 = pyodbc.connect(connStr)
            cursor3 = cnxn3.cursor()
            tsql_q_prnam = "select PRNAM from OPENQUERY(PTS_PROG_MYSQL, 'SELECT PRNAM "
            tsql_q_prnam +=  'FROM programX.PAREA WHERE CHEID = "{cheid}"'.format(cheid = fsparea_cheid)
            tsql_q_prnam += ' AND PRSDT <= "{bdate}" AND PREDT >= "{edate}"'.format(bdate = playlist[i][1], edate = playlist[i][2])
            tsql_q_prnam += ' AND PRTMS <= "{btime}" AND PRTME >= "{etime}"'.format(btime = playlist[i][3], etime = playlist[i][4])
            tsql_q_prnam += " AND  {week_of_day} = 1 order by PRENO desc')".format(week_of_day = playlist[i][6])

            tsql_q_prnam.format(cheid = fsparea_cheid, bdate = playlist[i][1], 
            edate = playlist[i][2], btime = playlist[i][3], etime = playlist[i][3],
            week_of_day = playlist[i][6])
            
            #print(tsql_q_prnam)
            with cursor3.execute(tsql_q_prnam):
                row = cursor3.fetchone()
                if row:
                    playlist[i][5] = str(row[0])
            
            cnxn3.close()

            #end of if block


    for p in playlist:
        print(p)







def main(argv):
    DO_EXPORT_BY_CHANNEL = False

    try:
        opts, args = getopt.getopt(argv, "", ["channel="])

    except Exception as e:
        template = "Exception {0} occur : {1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)

        sys.exit(2)

    for opt, arg in opts:
        if (opt == "--channel"):
            DO_EXPORT_BY_CHANNEL = True
            channel = arg


    if (DO_EXPORT_BY_CHANNEL):
        export_mod_prog_list(channel)


if (__name__ == "__main__"):
    main(sys.argv[1:])