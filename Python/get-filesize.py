#!/usr/bin/python

import sys
import os
import subprocess
import re
import datetime

"""
Usage :
get-filesize.py path | [path]
get-filesize.py target_dir.txt
get-filesize.py
"""
#filePattern = ".*\.mxf$"
filePattern = ".*"

TARGET_DIR = []
if (len(sys.argv) > 1):
    infile = sys.argv[1]
    if (os.path.isfile(infile)):
            fh = open(infile, "r")
            lines = fh.readlines()
            for l in lines:
                l = l.rstrip()
                TARGET_DIR.append(l)
    else:
        for p in sys.argv[1:]:
            TARGET_DIR.append(p)
else:
    #TARGET_DIR = ['/ptstv', '/hakkatv', '/mvtv', '/titv', '/news/pts', '/news/hakka', '/news/titv']
    TARGET_DIR = ['/Media/Doc/ptstv', '/Media/Doc/hakkatv', '/Media/Doc/titv']

now = datetime.datetime.now()
reExt = re.compile(filePattern, re.IGNORECASE) #find *.mxf files
for TARGET in TARGET_DIR:
    totalFileSize = 0
    count = 0
    print("Calculate " + TARGET)
    for root, dirs, files in os.walk(TARGET):
       #print "ROOT DIR = " + root
       for f in files:
          fPath = os.path.join(root, f)
          #fileName, extName = os.path.splitext(fPath)
          if (reExt.match(fPath) is None):
              #print fPath + " not match"
              continue

          statInfo = os.stat(fPath)
          totalFileSize += statInfo.st_size
          count += 1

    totalFileSize = totalFileSize / 10**9
    print("Total number of files :" + str(count) + " files")
    print("Total file size :" + str(totalFileSize) + " GB")
