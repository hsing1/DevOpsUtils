
import subprocess
import glob
import sys
import os
import re
import datetime
import time
import glob
import config
import ptsutils3
import getopt

HSM = config.HSM_CONFIG.HSM4
login = config.HSM_CONFIG.login
passwd = config.HSM_CONFIG.password
tsm_login = config.TSM_CONFIG.tsm_user1[0]
tsm_passwd = config.TSM_CONFIG.tsm_user1[1]

print(sys.version)

#get vol number
def check_file_vol(file_no):
    #file_no = 'G200928508260001' #in J00022L4
    #vol_list = []
    vol_set = set() 
    if (file_no[0] == 'P' or file_no[0] == 'D'): #only grep by file number
        file_no = file_no[0:12]

    #grep_cmd = 'grep ' + file_no + ' /mnt/GPFS/VOL/*'
    grep_cmd = 'grep ' + file_no + ' /mnt/MAM_ARCHIVE/TSM_INFO/VOL/HSM/*'
    plink_args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd] 
    grep_args = ['cd /mnt/MAM_ARCHIVE/TSM_INFO/VOL/HSM;', 'grep', file_no, '*']
    plink_args.extend(grep_args)
    proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
    reObj = re.compile(r'[A-Z]{1}\d{5}L4')
    lines = proc.stdout.readlines()
    if (len(lines) == 0):
        print(file_no + ":" + "No volumn match")
        vol_set.add("NONE")
        return vol_set

    for l in lines:
        l = l.decode('utf-8')
        #l = str(l)
        tmplist = re.split(r'\s+', l)
        if (len(l) > 1):
            res = reObj.match(tmplist[0])
            if (res):
                vol = res.group(0)
                #vol_list.append(vol)
                vol_set.add(vol)
                print(file_no + ":" + vol)
            else:
                print(file_no + ":" + "no volumn match")

    return vol_set


def test(case):

    if (case == 1):
        args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd, 'ls', '-a']
        proc = subprocess.Popen(args, stdout = subprocess.PIPE, shell = True)
        res = proc.stdout.readlines()
        print(res)
    elif (case == 2): # test dsmadmc command : query path
        plink_args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd] 
        linux_args = ['cd',  '/mnt/GPFS/VOL;']
        dsmadmc_args = ['dsmadmc', '-id={0}'.format(tsm_login), '-password={0}'.format(tsm_passwd), 'query path']
        #dsmadmc_args = ['which', 'dsmadmc']
        plink_args.extend(linux_args)
        plink_args.extend(dsmadmc_args)
        print(plink_args)
        proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
        lines = proc.stdout.readlines()
        for l in lines:
            l = l.rstrip()
            print(l)

    elif (case == 3): #query libvol TS3500
        print("Test case " + str(case) + " :query libvol TS3500")
        vol_list = []
        count = 0
        outfile = "ts3500.txt"
        outfile1 = "ts3500_vol.txt"
        fh = open(outfile, 'w')
        fh1 = open(outfile1, 'w')
        plink_args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd] 
        dsmadmc_args = ['dsmadmc', '-id={0}'.format(tsm_login), '-password={0}'.format(tsm_passwd), 'query', 'libvol', 'TS3500']
        plink_args.extend(dsmadmc_args)
        proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
        lines = proc.stdout.readlines()
        reObj = re.compile(r'[A-Z]{1}\d{5}L4')
        for l in lines:
            l = str(l)
            fh.writelines(l + "\n")
            tmplist = re.split(r'\s+', l)
            if (len(tmplist) > 1):
                res = reObj.match(tmplist[1])
                if (res):
                    vol = res.group(0)
                    vol_list.append(vol)
                    fh1.writelines(vol + "\n")
                    count += 1

        fh.close()
        fh1.close()
        print("Total :" + str(count))

    elif (case == 4): #get unavail volume : query vol access=unavailable
        print("Test case " + str(case) + " : query vol access=unavailable")
        vol_list = []
        time_stamp = datetime.datetime.now()
        date_time = time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime())
        outf = "unavail-" + date_time + ".txt"
        plink_args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd] 
        dsmadmc_args = ['dsmadmc', '-id={p}'.format(p = tsm_login), '-password={p}'.format(p = tsm_passwd), 'query', 'vol', 'access=unavailable']
        plink_args.extend(dsmadmc_args)
        #print(plink_args)
        proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
        lines = proc.stdout.readlines()
        reObj = re.compile(r'[A-Z]{1}\d{5}L4')
        for l in lines:
            l = l.decode('utf-8')
            #l = str(l)
            #print(l)
            tmplist = re.split(r'\s+', l)
            if (len(l) > 1):
                res = reObj.match(tmplist[0])
                if (res):
                    vol = res.group(0)
                    vol_list.append(vol)
                    print(vol)

    elif (case == 5): # check vol number by given file number
        file_no = 'G200928508260001' #in J00022L4
        grep_cmd = 'grep ' + file_no + ' /mnt/GPFS/VOL/*'
        plink_args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd] 
        grep_args = ['cd /mnt/GPFS/VOL;', 'grep', file_no, '*']
        plink_args.extend(grep_args)
        proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
        reObj = re.compile(r'[A-Z]{1}\d{5}L4')
        lines = proc.stdout.readlines()
        for l in lines:
            l = l.decode('utf-8')
            #l = str(l)
            tmplist = re.split(r'\s+', l)
            if (len(l) > 1):
                res = reObj.match(tmplist[0])
                if (res):
                    vol = res.group(0)
                    print(vol)

    elif (case == 6):
        #res = []
        res = set() 
        infile = sys.argv[2]
        fh = open(infile, "r")
        lines = fh.readlines()
        for l in lines:
            file_no = l.rstrip()
            res.update(check_file_vol(file_no))

        print("\n\nResult:")
        for v in res:
            print(v)
        fh.close()
    elif (case == 7): #query libvol TS3500
        vol_list = ptsutils3.get_libvol("TS3500")
        for v in vol_list:
            print(v)
        print("Total vol in TS3500: " + str(len(vol_list)))

    elif (case == 8):
        vol_list = ptsutils3.get_unavail_vol()
        for v in vol_list:
            print(v)

        print("Total unavail : " + str(len(vol_list)))

# end of test()

def check_by_file_list_1(flist):
    res = set() 
    status_dict = {}
    status_list = []
    for l in flist:
        file_no = ptsutils3.get_file_no(l)
        res.update(check_file_vol(file_no))

    status_list = check_tape_status(res)

    return status_list

def check_by_file_list(infile):
    res = set() 
    status_dict = {}
    status_list = []
    fh = open(infile, "r")
    flist = []
    lines = fh.readlines()
    for l in lines:
        file_no = ptsutils3.get_file_no(l.rstrip())
        flist.append(file_no)
        res.update(check_file_vol(file_no))

    status_list = check_tape_status(res)

    fh.close()
    return flist

#res_set = set([vol list])
def update_off_site_vol_list(vol_set, off_site_file=None):
    re_obj = re.compile(r'([A-Z]\d{5}L4).*')
    lib_vol_list = ptsutils3.get_libvol("TS3500") 
    unavail_vol_list = ptsutils3.get_unavail_vol()

    if (off_site_file == None):
        off_site_file = config.LOG.TSM_VOL_STATUS

    print("Update volume status in %s" %off_site_file)
    if (os.path.isfile(off_site_file)):
        fh = open(off_site_file, "r", encoding="utf-8")

        lines = fh.readlines()
        #reading previous vol in status file
        for l in lines:
            res = re_obj.match(l.rstrip())
            if (res):
                vol = res.group(1)
                #print(vol)
                vol_set.add(vol)

        fh.close()

    fh = open(off_site_file, "w", encoding="utf-8")
    on_u_temp = "{0} : on-site, unavailable\n"
    on_a_temp = "{0} : on-site, available\n"
    off_u_temp = "{0} : off-site, unavailable\n"
    off_a_temp = "{0} : off-site, available\n"
    #check vol status, and update log
    for v in vol_set:
        if (v in lib_vol_list and v in unavail_vol_list):
            mesg = on_u_temp.format(v)
            #print(mesg)
            fh.write(mesg)
            status = 'o-u'
            STATUS = 'UNAVALABLE'
        elif (v in lib_vol_list and v not in unavail_vol_list):
            mesg = on_a_temp.format(v)
            status = 'o-a'
        elif (v not in lib_vol_list and v in unavail_vol_list):
            mesg = off_u_temp.format(v)
            #print(mesg)
            fh.write(mesg)
            status = 'off-u'
            STATUS = 'OFF-SITE, UNAVAILABLE'
        else:
            mesg = off_a_temp.format(v)
            #print(mesg)
            fh.write(mesg)
            status = 'off-a'
            STATUS = 'OFF-SITE,AVAILABLE'

    fh.close()


#check tape library status
#todo : handle path with multiple volumes
def check_tape_status(vol_set):
    STATUS = ''
    status_dict = {} 
    lib_vol_list = ptsutils3.get_libvol("TS3500") 
    unavail_vol_list = ptsutils3.get_unavail_vol()
    vol_status = []
    print("\n\nResult:")
    for v in vol_set:
        if (v == 'NONE'):
            status = 'n/a'
            STATUS = 'NONE'
        elif (v in lib_vol_list and v in unavail_vol_list):
            print("{0} : on-site, unavailable".format(v))
            status = 'o-u'
            STATUS = 'UNAVALABLE'
        elif (v in lib_vol_list and v not in unavail_vol_list):
            print("{0} : on-site, available".format(v))
            status = 'o-a'
            STATUS = 'AVAILABLE'
        elif (v not in lib_vol_list and v in unavail_vol_list):
            print("{0} : off-site, unavailable".format(v))
            status = 'off-u'
            STATUS = 'OFF-SITE, UNAVAILABLE'
        else:
            print("{0} : off-site, available".format(v))
            status = 'off-a'
            STATUS = 'OFF-SITE,AVAILABLE'

        status_dict[v] = status
    vol_status = [STATUS, status_dict]

    update_off_site_vol_list(vol_set)
    
    return vol_status 

#check single file, get volume number
def check_by_file_no(file_path):
    print("DO check_by_file_no")
    print("Check volume status by file number ...")
    res = set() 
    status_dict = {}
    status_list = []
    file_no = ptsutils3.get_file_no(file_path)  # news id is support
    res.update(check_file_vol(file_no))
    #status_dict = check_tape_status(res)
    status_list = check_tape_status(res)

    return status_list

def usage():
    usage_str = """
Check file status
SYNOPSIS
    check_file_status [-f file_no.txti] [--tcase=case_num] [--id=file_no]
Function type:
    -f : sprcify input file contain file number list
    --tcase [cae number]
    -h : help
    --id=file_no
    --vol=VOLUME_ID
"""
    print(usage_str)

def main(argv):
    DO_CHECK_BY_LIST = False
    DO_CHECK_BY_FILE_NO = False
    DO_CHECK_BY_FILE_PATH = False
    DO_CHECK_VOL_STATUS = False
    DO_TEST_CASE = False
    DO_UPDATE_VOL_STATUS = False
    DO_HELP = False

    try:
        opts, args = getopt.getopt(argv, "hf:", ["update_vol_status=", "file_path=", "vol=", "file=", "id=", "tcase="])
    except getopt.GetoptError as err:
        print(str(err))
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if (opt in ("-f", "--file")):
            file_name = arg
            DO_CHECK_BY_LIST = True
        elif (opt in ("--vol")):
            vol_name = arg
            DO_CHECK_VOL_STATUS = True
        elif (opt in ("--id")):
            file_no = arg
            DO_CHECK_BY_FILE_NO = True
        elif (opt in ("--file_path")):
            file_path = arg
            DO_CHECK_BY_FILE_PATH = True
        elif (opt in ("--tcase")):
            test_case = int(arg)
            DO_TEST_CASE = True
        elif opt in ("--update_vol_status"):
            vol_name = arg
            DO_UPDATE_VOL_STATUS = True
        elif (opt in ("-h")):
            DO_HELP = True


    if (DO_CHECK_BY_LIST):
        check_by_file_list(file_name)
    elif (DO_CHECK_BY_FILE_NO):
        check_by_file_no(file_no)
    elif (DO_CHECK_BY_FILE_PATH):
        check_by_file_no(file_path)
    elif (DO_TEST_CASE):
        test(test_case)
    elif (DO_CHECK_VOL_STATUS):
        vol_set = set(vol_name.split(','))
        check_tape_status(vol_set)
    elif (DO_UPDATE_VOL_STATUS):
        vol_set = set(vol_name.split(','))
        update_off_site_vol_list(vol_set)
    elif (DO_HELP):
        usage()
        sys.exit(2)


if (__name__ == "__main__"):
    main(sys.argv[1:])