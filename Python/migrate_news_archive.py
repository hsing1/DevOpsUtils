# Migrate News files from \\10.13.200.98\mamspace\News to \\10.13.200.97\mam_data\NewsMamArchive
# Total 3272 files, size 19.8 TB
# create migrate list : HN20170103000001, \\10.13.200.97\mam_data\NewsMamArchive\Hakka\2017\01\03\HN20170103000001.mxf
# -*- coding: UTF-8 -*-
import os
import sys
import pyodbc
import config
from datetime import datetime, timedelta, date
import shutil
import getopt
import platform
import subprocess

version =  '20181025'
server = config.NEWNEWS_CONFIG.db_server
database = config.NEWNEWS_CONFIG.database 
username = config.NEWNEWS_CONFIG.db_user[0]
password = config.NEWNEWS_CONFIG.db_user[1]
#conn_str = 'DRIVER={{ODBC Driver 13 for SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
conn_str = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)

def query_video_meta():
    cnxn = pyodbc.connect(conn_str)
    cursor = cnxn.cursor()
    tsql = ("select VideoID, ArchivePath, ArchiveName, MP4Path, MP4Name from VideoMeta where AlreadyDelete = 0;")

    print("Get arhive information form DB ...")
    #query all data save in archive_info, key = ArchiveName
    count = 0
    archive_info = {}
    with cursor.execute(tsql):
        row = cursor.fetchone()
        while row:
            video_id = str(row[0])
            archive_path = str(row[1])
            archive_name = str(row[2])
            mp4_path = str(row[3])
            mp4_name = str(row[4])

            archive_file = os.path.join(archive_path, archive_name)
            mp4_file = os.path.join(mp4_path, mp4_name)
        
            path_list = []
            path_list.append(archive_file)
            path_list.append(mp4_file)
            archive_info[archive_name] = path_list
            
            row = cursor.fetchone()
    cnxn.close() #end of query_video_meta()
    return archive_info
    
      

# traverse archive folder, check VideoMeta.ArchivePath in DB
def check_news_archive_file(archive_info, archive_dir):
    
    fh = open('check_news_archive_file.log', 'w', encoding='utf-8')
    print("Check " + archive_dir)
    db_path = ''
    for r, ds, fs in os.walk(archive_dir):
        for f in fs:
            archive_name = f
            phy_path = os.path.join(r, f)
            try:
                path_list = archive_info[archive_name]
                db_path = path_list[0]
                if (phy_path != db_path):
                    err_msg1 = 'File path mismatch:\n'
                    err_msg2 = 'Physical path:' + phy_path + '\n'
                    err_msg3 = '      DB path:' + db_path + '\n'
                    fh.writelines(err_msg1)
                    fh.writelines(err_msg2)
                    fh.writelines(err_msg3)

            except Exception as e:
                template = "Exception {0}: {1}\n"
                err_msg = template.format(type(e).__name__, e.args)
                print(err_msg) 
                fh.writelines(err_msg)


# -a : check if file exits
def check_file_exist_from_db(archive_info):
    print("Check file path from DB")
    log =  "check_news_archive_path.log"
    print("Log=" + log)
    f = open(log, "w", encoding = 'utf-8')
    count = 0
    total = len(archive_info.keys())
    print("Check " + str(total) + " files ...")
    for news_id in archive_info.keys():
        path_list = archive_info[news_id]
        archive_file = path_list[0]
        mp4_file = path_list[1] 

        if ((count % 1000 ) == 0):
            print(str(count) + ":Check " + archive_file)

        if (not os.path.isfile(archive_file)):
            msg = "{0} {1} not found\n".format(news_id, archive_file)
            f.writelines(msg)
            print(msg)
        
        if (not os.path.isfile(mp4_file)):
            msg = "{0} {1} not found\n".format(news_id, mp4_file)
            f.writelines(msg)
            print(msg)

        count += 1


# copy src_dir to target_dir (use OS copy), then invoke this function to update DB
# compare src_dir and target_dir, if file exist in src_dir and target_dir, then update archive path in DB
def update_archive_path(archive_info, src_dir, target_dir):
    cnxn = pyodbc.connect(conn_str)
    cursor = cnxn.cursor()
    update_tsql = ("update VideoMeta set ArchivePath = ? where ArchiveName = ?;")

    #SOURCE_DIR = '\\\\10.13.200.98\\mamspace\\News\\Hakka\\2016\\12\\20'
    #TARGET_DIR = '\\\\10.13.200.97\\mam_data\\migrate_from_200_98\\Hakka\\2016\\12\\20'
    SOURCE_DIR = src_dir
    TARGET_DIR = target_dir
    print("Check:" + SOURCE_DIR + " ==> " + TARGET_DIR)
    #return

    fh_log = open("migrate_news_archive.log", "a", encoding="utf-8")
    is_sync = False
    count = 0
    for r, ds, fs in os.walk(SOURCE_DIR):
        for f in fs:
            is_sync = False
            count += 1
            res = os.path.splitext(f)
            base_name = res[0]
            
            #print("---- Check " + f)
            spath = os.path.join(r, f)
            tpath = spath.replace(SOURCE_DIR, TARGET_DIR)
            if (os.path.exists(tpath)): # physical path, come from file system
                #check file size
                stat_info = os.stat(spath)
                size_src = stat_info.st_size
                stat_info = os.stat(tpath)
                size_target = stat_info.st_size
                if (size_src != size_target):
                    err_msg1 = "Error : size not match"
                    err_msg2 = spath + "(" + str(size_src) + ")"
                    err_msg3 = tpath + "(" + str(size_target) + ")"
                    print(err_msg1)
                    print(err_msg2)
                    print(err_msg3)
                    fh_log.writelines(err_msg1)
                    fh_log.writelines(err_msg2)
                    fh_log.writelines(err_msg3)
                    continue

                # replace new path in DB VideoMeta
                #print("Found " + tpath)
                is_sync = True
                # get path from DB
                path_list = archive_info[f]
                archive_file = path_list[0]
                mp4_file = path_list[1]
                #print("Archive file " + archive_file)
                #print("mp4 file " + mp4_file)
                new_path = archive_file.replace(SOURCE_DIR, TARGET_DIR)
                path_list.append(new_path)
                #print("New path " + new_path)
                #check if new_path file is exists. (double check), if yes, update 
                if (os.path.isfile(new_path)):
                    archive_path = os.path.dirname(archive_file) 
                    archive_name = f 
                    new_archive_path = os.path.dirname(new_path)
                    #check if migrated file path has been updated in DB
                    if (archive_path != new_archive_path):
                        timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S") 
                        message1 = timestamp + " : update path for " + archive_name + ":\n"
                        message2 = archive_path + " --> " + new_archive_path + "\n"
                        print(message1)
                        print(message2)
                        fh_log.writelines(message1)
                        fh_log.writelines(message2)
                        try:
                            cursor.execute(update_tsql, new_archive_path, archive_name)
                            cnxn.commit()
                        except Exception as e:
                            err_msg = "Error : " + str(e) + "\n"
                            print(err_msg)
                            fh_log.writelines(err_msg)
                            cnxn.rollback()
            else: #file is not migrated
                #log message
                timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S") 
                err_msg = timestamp + " Error : file not found: " + tpath 
                print(err_msg)
                fh_log.writelines(err_msg)

    cursor.close()
    fh_log.close()
# end of update_archive_path()

#compare src_dir and target_dir, for general purpose
def compare_dir(src_dir, target_dir):
    print("Compare " + src_dir + " and " + target_dir)

    fh_log = open("compare_news.log", "w", encoding='utf-8') 

    for r, ds, fs in os.walk(src_dir):
        for f in fs:
            spath = os.path.join(r, f)
            tpath = spath.replace(src_dir, target_dir)

            if (os.path.isfile(tpath)):
                stat_info = os.stat(spath)
                size_src = stat_info.st_size
                stat_info = os.stat(tpath)
                size_target = stat_info.st_size
                if (size_src != size_target):
                    err_msg1 = "Error : size not match"
                    err_msg2 = spath + "(" + str(size_src) + ")"
                    err_msg3 = tpath + "(" + str(size_target) + ")"
                    print(err_msg1)
                    print(err_msg2)
                    print(err_msg3)
                    fh_log.writelines(err_msg1)
                    fh_log.writelines(err_msg2)
                    fh_log.writelines(err_msg3)
            else:
                print(spath + " not migrated")
                fh_log.writelines(spath + "\n")

    fh_log.close()

# compare src_dir/*.mxf.bak target_dir/*.mxf
def check_backup_file_to_delete(bak_dir):
    print("Check " + bak_dir)
    spath = ''
    tpath = ''
    PASS = True
    FOUND_BAK_FILE = False

    archive_info = query_video_meta()

    fh_log = open("compare_news.log", "w", encoding='utf-8') 

    for r, ds, fs in os.walk(bak_dir):
        for f in fs:
            if (not f.endswith('.bak')):
                continue

            FOUND_BAK_FILE = True

            spath = os.path.join(r, f)
            rm_file = spath + '.rm'

            if (not os.path.isfile(rm_file)):
                msg = "Warning : {p} has no mark file *.rm\n".format(p = spath)
                fh_log.writelines(msg)
                PASS = False

                #reset *.mxf.bak to *.mxf for migrate again
                org_path, ext = os.path.splitext(spath)
                os.rename(spath, org_path)
                continue

            f_name, ext = os.path.splitext(f)
            path_list = archive_info[f_name]
            archive_path = path_list[0]
            mp4_file = path_list[1] 

            if (os.path.isfile(archive_path)):
                stat_info = os.stat(spath)
                size_src = stat_info.st_size
                stat_info = os.stat(archive_path)
                size_target = stat_info.st_size
                if (size_src != size_target):
                    PASS = False
                    err_msg1 = "Error : size not match"
                    err_msg2 = spath + "(" + str(size_src) + ")"
                    err_msg3 = tpath + "(" + str(size_target) + ")"
                    print(err_msg1)
                    print(err_msg2)
                    print(err_msg3)
                    fh_log.writelines(err_msg1)
                    fh_log.writelines(err_msg2)
                    fh_log.writelines(err_msg3)

                    #reset *.mxf.bak to *.mxf for migrate again
                    org_path, ext = os.path.splitext(spath)
                    os.rename(spath, org_path)
            else:
                PASS = False
                print(spath + " not migrated")
                fh_log.writelines(spath + "\n")
                #reset *.mxf.bak to *.mxf for migrate again
                org_path, ext = os.path.splitext(spath)
                os.rename(spath, org_path)

    fh_log.close()
    if (FOUND_BAK_FILE):
        if (PASS):
            print("All files in %s is migrate" %bak_dir)
        else:
            print("Some files in %s is not migrate properly" %bak_dir)
    else:
        print("No file in %s is migrated" %bak_dir)




# move archived files to target folder
# update ArchivePath in DB
def migrate_archived_file(src_dir, target_dir):
    cnxn = pyodbc.connect(conn_str)
    cursor = cnxn.cursor()
    update_tsql = ("update VideoMeta set ArchivePath = ? where ArchiveName = ?;")
    #SOURCE_DIR= '\\\\10.13.200.97\\mam_data\\migrate_from_200_98\\Hakka\\2016\\12\\20'
    #TARGET_DIR = '\\\\10.13.200.97\\mam_data\\NewsArchive\\Hakka\\2016\\12\\20'
    SOURCE_DIR = src_dir
    TARGET_DIR = target_dir
    print("Migrate:" + SOURCE_DIR + " ==> " + TARGET_DIR)
    #return

    fh_log = open("migrate_news_archive.log", "a", encoding="utf-8")
    count = 0
    for r, ds, fs in os.walk(SOURCE_DIR):
        for f in fs:
            count += 1
            res = os.path.splitext(f)
            base_name = res[0]
            ext = res[1].lower()

            if (ext != '.mxf'):
                continue
            
            #print("---- Check " + f)
            spath = os.path.join(r, f)
            tpath = spath.replace(SOURCE_DIR, TARGET_DIR)
            timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S") 
            if (os.path.exists(tpath)):
                is_sync = True
                #no need to migrate file
                #log message
                message1 = timestamp + " : SKIP " + spath + ": already found in " + tpath
                print(message1)
                fh_log.writelines(message1 +"\n")
             
            else:
                # migrate src_file to target_file
                # replace new path in DB VideoMeta
                try:
                    # copy file to target_dir
                    #shutil.copy2(spath, tpath)

                    archive_path = os.path.dirname(spath) 
                    archive_name = f 
                    new_archive_path = os.path.dirname(tpath)
                    if (not os.path.isdir(new_archive_path)):
                        try:
                            os.makedirs(new_archive_path)
                        except Exception as e1:
                            template = "{0}:Exception {1} occur : {2!r}(process {3})"
                            err_msg = template.format(timestamp, type(e1).__name__, e1.args, new_archive_path)
                            print(err_msg)
                            fh_log.writelines(err_msg + "\n")
                            print("Skip make directory " + new_archive_path)
                            continue

                    try: #copy source file to target folder
                        message1 = timestamp + " : migrate " + archive_name + ":"
                        message2 = spath + " --> " + tpath
                        print(message1)
                        print(message2)
                        cmd = "copy {s} {t}".format(s = spath, t = tpath)
                        #proc = subprocess.Popen(cmd) no exception trown
                        #os.system("copy " + spath + " " + tpath) cannot throw exception
                        shutil.copy2(spath, tpath)
                        fh_log.writelines(message1)
                        fh_log.writelines(message2 + "\n")

                        timestamp2 = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S") 
                        try: # update ArchivePath in DB
                            cursor.execute(update_tsql, new_archive_path, archive_name)

                            #rename original file
                            new_file = spath + ".bak"
                            os.rename(spath, new_file)
                            cnxn.commit()
                            message1 = timestamp2 + " : update path for " + archive_name + ":\n"
                            message2 = archive_path + " --> " + new_archive_path + "\n"
                        except Exception as e2:
                            template = "{0}:Exception {1} occur : {2!r}"
                            err_msg = template.format(timestamp2, type(e2).__name__, e2.args)
                            print(err_msg)
                            fh_log.writelines(err_msg)
                            cnxn.rollback()
                            continue

                    except Exception as e3:
                        template = "{0} Exception {1} occur : {2!r} (process {3})"
                        err_msg = template.format(timestamp, type(e3).__name__, e3.args, spath)
                        print(err_msg)
                        print("Skip copy " + spath)
                        fh_log.writelines(err_msg + "\n")
                        cnxn.rollback()
                        continue

                except Exception as e4:
                    err_msg = timestamp + " Error : " + str(e4) + "\n"
                    print(err_msg)
                    fh_log.writelines(err_msg)
                    cnxn.rollback()

    cursor.close() 
    fh_log.close()
# end of migrate_archived_file()

# -k : mark migrated files by creating *.rm file
def mark_migrated_file(archive_info, src_dir, target_dir):
    org_path = ''
    print("Mark remove files:" + src_dir)

    fh_log = open("mark_news_archive.log", "a", encoding="utf-8")
    count = 0
    for r, ds, fs in os.walk(src_dir):
        print("---- Check " + r)
        for f in fs:
            FILE_IS_COPIED = False
            DB_IS_UPDATED = False
            count += 1
            res = os.path.splitext(f)
            ext = res[1].lower()
            if (ext != '.bak'):
                continue
            
            spath = os.path.join(r, f)
            org_path = spath.rstrip('.bak')
            tpath = org_path.replace(src_dir, target_dir)
            #compare file size
            if (os.path.exists(tpath)):
                stat_info = os.stat(spath)
                src_file_size = stat_info.st_size
                stat_info = os.stat(tpath)
                target_file_size = stat_info.st_size
                if (src_file_size != target_file_size):
                    template = "File size not match\n{0}({1})\n{2}({3})\n"
                    err_msg = template.format(spath, str(src_file_size), tpath, str(target_file_size))
                    print(err_msg)
                    fh_log.writelines(err_msg)
                else:
                    FILE_IS_COPIED = True
            else:
                timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S") 
                template = "{0}:File not migrated {1}\n"
                err_msg = template.format(timestamp, spath)
                print(err_msg)
                fh_log.writelines(err_msg)
            
            #check DB info
            try:
                archive_name = f.rstrip('.bak') 
                path_list = archive_info[archive_name]
                db_path = path_list[0]
                if (db_path == org_path):
                    timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S") 
                    template = "{0}:DB path is not updated: {1}\n"
                    msg = template.format(timestamp, db_path)
                    print(msg)
                    fh_log.writelines(msg)
                elif (db_path == tpath):
                    DB_IS_UPDATED = True
                else:
                    timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S") 
                    template = "{0}:Fatal : DB path is not correct {1}\n"
                    err_msg = template.format(timestamp, db_path)
                    print(err_msg)
                    fh_log.writelines(err_msg)


            except Exception as e:
                template = "Exception {0}:{1}\n"
                err_msg = template.format(type(e).__name__, e.args)
                print(err_msg)
                fh_log.writelines(err_msg)

            if (FILE_IS_COPIED and DB_IS_UPDATED):
                template = "Mark for removing migrated file {0}\n"
                msg = template.format(spath)

                #crete mark file for removing job
                fh_rm = open(spath + ".rm", "w")
                fh_rm.close()
                #print(msg)

    fh_log.close()
# end of migrate_archived_file()

# -r : remove file with *.rm file
def remove_migrated_file(src_dir, delta_i):
    print("Purge:" + src_dir)

    delta_day = timedelta(days = delta_i)
    today = date.today()

    fh_log = open("remove_news_archive.log", "a", encoding="utf-8")
    for r, ds, fs in os.walk(src_dir):
        print("---- Check " + r)
        if (len(os.listdir(r)) == 0):
            print("Found empty folder and remove " + r)
            shutil.rmtree(r)

        for f in fs:
            res = os.path.splitext(f)
            ext = res[1]
            if (ext == '.rm'):
                continue
            elif (ext == '.bak'): #need compare file size?
                spath = os.path.join(r, f)
                rm_file = spath + ".rm"
                if (os.path.isfile(rm_file)):
                    if (platform.system() == 'Windows'):
                        stat_info = os.stat(rm_file)
                        ctime = os.path.getctime(rm_file)
                        cdate = date.fromtimestamp(ctime)
                        diff_day = today - cdate
                        #print(diff_day)
                        #print(delta_day)
                        if (diff_day >= delta_day):
                            print("Remove " + spath)
                            os.remove(spath)
                            os.remove(rm_file)
                            timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S") 
                            template = "{0}:remove {1}\n"
                            msg = template.format(timestamp, spath)
                            fh_log.writelines(msg)
    fh_log.close()
# end of migrate_archived_file()

def usage():
    usage_str = """
Migrate news files to new folder, and update archive path in DB
Usage : migrate_news_archive.exe function_type -s <src_dir> -t <target_dir>
Function type:
   -c : check migrated files
   -d : diff files between source folder and target folder
   -k : mark migrated files for being removed
   -m : migrat files from source folder to target folder
   -r : remove files with *.rm filr in source
   -u : update DB
   -v : print version

-s, --src_dir=SRC_DIR : sprcify source folder to be migrated
-t, --target_dir=SRC_DIR : sprcify target folder
    --delta=NUM : remove files that have been migrated for NUM days

Ex : To migrate file from source folder to target folder
     migrate_news_archive.exe -m -s \\\\10.13.200.97\\mam_data\\migrate_from_200_98\\Hakka\\2017 -t \\\\10.13.200.97\\mam_data\\NewsMamArchive\\Hakka\\2017
"""
    print(usage_str)

def main(argv):
    DO_MIGRATE = False
    DO_CHECK = False
    DO_UPDATE_PATH = False
    DO_DIFF = False
    DO_REMOVE = False
    DO_MARK = False
    DO_VERSION_PRINT = False
    DO_CHECK_DB = False
    DO_CHECK_BAK_DIR = False
    s_dir = ''
    t_dir = ''
    bak_dir = ''

    try:
        opts, args = getopt.getopt(argv, "acdhkmrvs:t:", 
        ["src_dir=", "target_dir=", "delta=",
         "check_bak="])

    except Exception as e:
        template = "Exception {0} occur : {1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)
        usage()
        sys.exit(2)

    for opt, arg in opts:
        # get job tyype
        if (opt == '-m'):
            DO_MIGRATE = True
        elif (opt == '-c'):
            DO_CHECK = True
        elif (opt == '-d'):
            # -d  -s src_dir -t target_dir : diff migrated files
            DO_DIFF = True
        elif (opt == '-h'):
            usage()
            sys.exit()
        elif (opt == '-k'):
            DO_MARK = True
        elif (opt == '-r'): # remove migratred files
            DO_REMOVE = True
        elif (opt == '-u'):
            DO_UPDATE_PATH = True
        elif (opt == '-v'):
            DO_VERSION_PRINT = True
        elif (opt == '-a'): #check DB path
            DO_CHECK_DB = True
        elif (opt == "--check_bak"):
            DO_CHECK_BAK_DIR = True
            bak_dir = arg

        #else:
        #    print("dafault = -h")
        #    print("Migrate news files to new folder, and update archive path in DB")
        #    print("Usage : migrate_news_archive.exe -s <src_dir> -t <target_dir>")
        #    print("Ex : migrate_news_archive.exe -s \\\\10.13.200.97\\mam_data\\migrate_from_200_98\\Hakka\\2017 -t \\\\10.13.200.97\\mam_data\\NewsMamArchive\\Hakka\\2017")
        #    sys.exit()


        if (opt in ("-s", "--src_dir")):
            s_dir = arg
            print("source=" + s_dir)
        elif (opt in ("-t", "--target_dir")):
            t_dir = arg
            print("target=" + t_dir)
        elif (opt in ("--delta")):
            delta = int(arg)
    

    if (DO_CHECK):
        print("Check archived files ...")
        archive_info = query_video_meta()
        check_news_archive_file(archive_info, t_dir)
    elif (DO_UPDATE_PATH): #just update DB path
        print("Update archive path ...")
        archive_info = query_video_meta()
        update_archive_path(archive_info, s_dir, t_dir)
    elif (DO_DIFF):
        compare_dir(s_dir, t_dir)
    elif (DO_MIGRATE):
        print("Migrate files ...")
        migrate_archived_file(s_dir, t_dir)
    elif (DO_REMOVE):
        if (os.path.exists(s_dir)):
            remove_migrated_file(s_dir, delta)
        else:
            print("Error: folder not found " + s_dir)
    elif (DO_MARK): #create *.rm file for delete
        archive_info = query_video_meta()
        mark_migrated_file(archive_info, s_dir, t_dir)
    elif (DO_VERSION_PRINT):
        print("Version:" +  version)
    elif (DO_CHECK_DB):
        print("Check DB ...")
        archive_info = query_video_meta()
        check_file_exist_from_db(archive_info)
    elif (DO_CHECK_BAK_DIR):
        check_backup_file_to_delete(bak_dir)




if (__name__ == "__main__"):
    main(sys.argv[1:])


# not used
def update_archive_path_old(archive_info):
    cnxn = pyodbc.connect(conn_str)
    cursor = cnxn.cursor()
    tsql = ("select VideoID, ArchivePath, ArchiveName, MP4Path, MP4Name from VideoMeta where ArchiveName = ?;")

    SOURCE_DIR = '\\\\10.13.200.98\\mamspace\\News\\Hakka\\2016\\12\\20'
    TARGET_DIR = '\\\\10.13.200.97\\mam_data\\migrate_from_200_98\\Hakka\\2016\\12\\20'
    fh_log = open("migrate_news_archive.log", "a", )
    is_sync = False
    count = 0
    for r, ds, fs in os.walk(SOURCE_DIR):
        for f in fs:
            is_sync = False
            count += 1
            res = os.path.splitext(f)
            base_name = res[0]
            
            print("---- Check " + f)
            spath = os.path.join(r, f)
            tpath = spath.replace(SOURCE_DIR, TARGET_DIR)
            if (os.path.exists(tpath)):
                # replace new path in DB VideoMeta
                print("Found " + tpath)
                is_sync = True
                path_list = archive_info[f]
                archive_file = path_list[0]
                mp4_file = path_list[1]
                print("Archive file " + archive_file)
                print("mp4 file " + mp4_file)
                new_path = archive_file.replace(SOURCE_DIR, TARGET_DIR)
                path_list.append(new_path)
                print("New path " + new_path)
                #check if new_path file is exists. if yes, update 
                if (os.path.isfile(new_path)):
                    with cursor.execute(tsql, f):
                        row = cursor.fetchone()
                        while row:
                            archive_path = str(row[1])
                            archive_name = str(row[2])
                            new_archive_path = os.path.dirname(new_path)
                            if (archive_path != new_archive_path):
                                timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S") 
                                message1 = timestamp + " : update path for " + f + ":\n"
                                message2 = archive_path + " --> " + new_archive_path + "\n"
                                print(message1)
                                print(message2)
                                fh_log.writelines(message1)
                                fh_log.writelines(message2)
    
                            row = cursor.fetchone()
    cursor.close()
    fh_log.close()
# end of update_archive_path()