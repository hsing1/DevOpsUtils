#!/usr/bin/python
# -*- coding: UTF-8 -*-
# Usage : check_libvol.py [vol_name | vol.txt]

import sys
import os
import ptsutils

input = sys.argv[1]
fh = None
vol = None

if (os.path.isfile(input)):
    fh = open(input, "r")
else:
    vol = input

#get all lib volume in tape library
#query libvol TS3500
vol_list = ptsutils.get_libvol('TS3500')

if (fh):
    lines = fh.readlines()
    for vol in lines:
        vol = vol.strip()
        if (vol in vol_list):
            print(vol + " : on-site")
        else:
            print(vol + " : off-site")
else:
    if (vol in vol_list):
        print(vol + " : on-site")
    else:
        print(vol + " : off-site")