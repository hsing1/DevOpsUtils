#!/usr/bin/python

import sys
import os
import re

DEBUG = 0
TARGET_ROOT = ["/news/pts", "/news/hakka", "/news/titv"]
fileNo = sys.argv[1]
outfile = "news.txt"
ofh = open(outfile, "w")

def check_news(files):
    reExt = re.compile(".*\.mxf$", re.IGNORECASE)
    for f in files:
        #if (reExt.match(f) is None):  #check if it is *.mxf
        #    continue

        (fileID, ext) = os.path.splitext(f)

        year = fileID[0:4]
        mon = fileID[4:6]
        day = fileID[6:8]

        #check if file already in GPFS
        inGPFS = 0
        for r in TARGET_ROOT:
            TARGET_DIR = r + "/" + year + "/" + mon + "/" + day + "/" + fileID + "/"
            print "check: " + TARGET_DIR
            if (os.path.exists(TARGET_DIR)):   #target dir already in GPFS
                print "Found file in " + TARGET_DIR
                cmd = "ls -l " + TARGET_DIR + "/" + f + ".*"
                os.system(cmd)
                filelist = os.listdir(TARGET_DIR)
                for f in filelist:
                    tf = TARGET_DIR + "/" + f
                    print tf
                    ofh.write(tf + "\n")
                inGPFS = 1
                break
            else:
                print "Cannot find " + TARGET_DIR


files = []
if (os.path.isfile(fileNo)):
    fh = open(fileNo)
    lines = fh.readlines()
    for l in lines:
        l = l.rstrip()
        files.append(l)
else:
    files.append(fileNo)

check_news(files)
