# -*- coding:UTF-8 -*-
import os
import sys
import getopt
import time


def sync_folder(source_dir, target_dir, exclude_dir = None):
    for r, ds, fs in os.walk(source_dir):
        if (r == exclude_dir):
            print("Skip " + r)
            continue

        for f in fs:
            spath = os.path.join(r, f)
            #print("Process " + spath)
            tpath = spath.replace(source_dir, target_dir)
            if  (not os.path.isfile(tpath)):
                try:
                    #cmd = 'copy "' + spath + '" "' + tpath + '"'
                    cmd = "copy \"{s}\" \"{t}\"".format(s=spath, t=tpath)
                    print(cmd)
                    os.system(cmd)
                except Exception as e:
                    template = "Exception {0} occur : {1!r}"
                    message = template.format(type(e).__name__, e.args)
                    print(message)


def watch_sync(source_dir, target_dir, exclude_dir =  None):
    print("DO watch_sync")
    while (1):
        sync_folder(source_dir, target_dir, exclude_dir)
        print("Sleeping ...")
        time.sleep(3600)



def main(argv):
    HAS_SOURCE_DIR = False
    HAS_TARGET_DIR = False
    DO_WATCH_MODE = False 
    source_dir = ''
    target_dir = ''
    exclude_dir = None 

    try:
        opts, args = getopt.getopt(argv, "s:t:", ["source=", "target=", "watch", "ex="])

    except Exception as e:
        template = "Exception {0} occur : {1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)

        sys.exit(2)

    for opt, arg in opts:
        print("Process " + opt + " " + arg)
        if (opt in ("-s", "--source")):
            source_dir = arg
            if (not os.path.isdir(source_dir)):
                print(source_dir + " not found")
            else:
                HAS_SOURCE_DIR = True
        elif (opt in ("-t", "--target")):
            target_dir = arg
            if (not os.path.isdir(target_dir)):
                print(target_dir + " not found")
            else:
                HAS_TARGET_DIR = True
        elif (opt == "--watch"):
            DO_WATCH_MODE = True
        
        elif (opt == "--ex"):
            exclude_dir = arg


    if (not DO_WATCH_MODE and HAS_SOURCE_DIR and HAS_TARGET_DIR):
        sync_folder(source_dir, target_dir, exclude_dir)
    elif (DO_WATCH_MODE and HAS_SOURCE_DIR and HAS_TARGET_DIR):
        watch_sync(source_dir, target_dir, exclude_dir)



if (__name__ == "__main__"):
    main(sys.argv[1:])