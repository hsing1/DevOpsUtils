#檢查 TBTSM_JOB 將狀態為 5 的 reset 為 0
# -*- coding: UTF-8 -*-
import pyodbc
import sys
import re
import config

in_file = sys.argv[1]

server = config.MAM_CONFIG.db_server
database = config.MAM_CONFIG.database
username = config.MAM_CONFIG.db_user2[0]
password = config.MAM_CONFIG.db_user2[1]
conn_str = 'DRIVER={{ODBC Driver 13 for SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
cnxn = pyodbc.connect(conn_str)
cursor = cnxn.cursor()


# extract JOBID from 呼叫TSM-ReCall,JOBID=334217	20150800865	UPLOAD	P201508008650004	002MQ0	\\mamstream\MAMDFS\ptstv\HVideo\2015\20150800865\P201508008650004.mxf	\\10.13.200.22\mamnas1\Approved\002MQ0.mxf	2018-08-28 07:28:00:18
fh = open(in_file, "r", encoding = "utf-8")
lines = fh.readlines()
reObj = re.compile(r'.*JOBID=(\d{6}).*')
tsm_job_list = []
for l in lines:
    l = l.strip()
    #print("check : "+ l)
    res = reObj.match(l)
    if (res):
        #print(res.group(0))
        #print(res.group(1))
        tsm_job_list.append(res.group(1))


reset_list = []
tsql = ("select FNTSMJOB_ID, FSSOURCE_PATH, FNSTATUS, FSNOTIFY_ADDR, FDREGISTER_TIME, FDLASTUPDATE_TIME, FSRESULT from TBTSM_JOB where FNTSMJOB_ID = ?")
for tsm_job_id in tsm_job_list:
    with cursor.execute(tsql, tsm_job_id):
        row = cursor.fetchone()
        while row:
            fntsmjob_id = str(row[0])
            fssource_path = str(row[1])
            fnstatus = str(row[2])
            fsnotify_addr = str(row[3])
            fdregister_time = str(row[4])
            fdlastupdate_time = str(row[5])
            fsresult = str(row[6])

            info = fntsmjob_id + "\n" + fnstatus + "\n" + fssource_path + "\n" + fsresult
            if (fnstatus == '5'):
                reset_list.append(fntsmjob_id)
            if (fnstatus != '3'):
                print(info)

            row = cursor.fetchone()
            
cnxn.close()