#!usr/bin/python

#import pyodbc
import os
import datetime
import sys
import ptsutils3

#Usage audio_test.py 2018-01-30 13

str_date = sys.argv[1]
str_channel = sys.argv[2]

#run in windows
server = '10.13.210.33'
nas1 = '10.13.200.22'
APPROVED_DIR = r'\\10.13.200.22\mamnas1\Approved'
#server = '10.13.220.34'
database = 'MAM'
username = 'audiotest' #db owner for executing SPL procedure
password = 'audiotest'

connStr = 'DRIVER={{ODBC Driver 13 for SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)

def get_playlist_all_test(pdate, channel):
    fvideo_id = "videoid-" + pdate + "-" + channel + ".txt"
    playlist = []
    video_id_dict = {}
    count = 0 #count queue for all str_channel
    count_ch = 0 #count queue number for specified channel
    cnxn = pyodbc.connect(connStr)
    cursor = cnxn.cursor()
    #tsql = ("exec SP_Q_GET_PLAY_VIDEOID_LIST")
    tsql = ("select * from VW_PLAYLIST_FROM_NOW order by FDDATE, FSCHANNEL_ID, QUEUE")

    fh = open(fvideo_id, "w")

    with cursor.execute(tsql):
        row = cursor.fetchone()
        while row:
            record = ""
            queueno = int(row[0])
            name = str(row[1]).rstrip()
            episode = int(row[2])
            fddate = str(row[3])
            fsplaytime = str(row[4])
            fschannel = str(row[5])
            video_id = str(row[6]).rstrip()
            queue = [queueno, name, episode, fddate, fsplaytime, fschannel, video_id]
            playlist.append(queue)
            if (fddate == pdate and fschannel == channel):
                if (len(video_id) > 0 and video_id != 'None' and video_id not in video_id_dict):  # use regular expression instead
                    video_id_dict[video_id[2:]] = queue
                    fh.writelines(video_id)
                    count_ch = count_ch + 1

            """
            for i in range(len(row)):
                record = record + str(row[i]).rstrip() + "|"
            print(record)
            """
            count = count + 1
            row = cursor.fetchone()
    for q in playlist:
        if (q[3] == pdate and q[5] == channel):
            print(q)
    print("Total number of queue : " + str(count))

    not_found = []
    for vid in video_id_dict.keys():
        print(vid + ":" + str(video_id_dict[vid]))
        target_file = APPROVED_DIR + "\\" + vid + ".mxf"
        if (os.path.isfile(target_file)):
            print ("Found " + target_file)
        else:
            print ("Cannot find " + target_file)
            not_found.append(vid)

    print("\n\nTotal number of queue (all channel): " + str(count))
    print("Total number of queue (ch-" + channel +  ", " + pdate + "): " + str(count_ch))
    print("Total number of file: " + str(len(video_id_dict)))
    print("\n\n--------------------------- File not found ---------------------------------")
    for vid in not_found:
        print(str(video_id_dict[vid]) + " not found")

    return video_id_dict


def comp_approved_atsin(video_id_dict, src_dir, target_dir):
    for id in video_id_dict.keys():
        print("Check " + id + "  ------")
        src_f = src_dir + "\\" + id + ".mxf"
        target_f = target_dir + "\\" + id + ".mxf"

        if (os.path.isfile(target_f)):
            #print("Found " + target_f)
            if (os.path.isfile(src_f)):
                print("remove " + src_f)
        else:
            print("Cannot find " + target_f)
            print(video_id_dict[id])
            if (os.path.isfile(src_f)):
                print("move file to Approved " + src_f)

def move_file_by_video_id_org(video_id_dict, src_dir, target_dir):
    for id in video_id_dict.keys():
        print("Check " + id + "  ------")
        src_f = src_dir + "\\" + id + ".mxf"
        target_f = target_dir + "\\" + id + ".mxf"

        if (os.path.isfile(target_f)):
            print("Found " + target_f)
        else:
            if (os.path.isfile(src_f)):
                print("move file to  " + target_f)
                try:
                    os.rename(src_f, target_f)
                except Exception:
                    print("Permission denied")
                    continue





def get_playlist_prog():

    #cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER=' + server + ';PORT=1443;DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
    #cnxn = pyodbc.connect(SERVER=' + server + ';PORT=1443;DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
    cursor = cnxn.cursor()

    print ('Reading data from TBUSERS')
    tsql = "select * from TBUSERS where FSUSER = 'hsing1';"
    tsql = ("select FSPROG_ID, FNEPISODE, FSPROG_NAME, FDDATE, FSPLAY_TIME from TBPGM_COMBINE_QUEUE " +
            "where FDDATE = '2018-01-11' and FSCHANNEL_ID = '12' order by FSPLAY_TIME;")
    tsql = ("select B.FSVIDEO_PROG, A.FSPROG_ID, A.FNEPISODE, A.FSPROG_NAME, A.FDDATE, A.FSPLAY_TIME from TBPGM_COMBINE_QUEUE A left join TBLOG_VIDEO B on A.FSVIDEO_ID = B.FSVIDEO_PROG " +
             "where FDDATE between '2018-01-25' and '2018-01-26' and A.FSCHANNEL_ID in ('12', '13', '14', '07') order by A.FSPLAY_TIME")


    count = 0
    main_prog = []
    playlist = []
    queue = []
    video_id_dict = {}

    with cursor.execute(tsql):
        row = cursor.fetchone()
        print(len(row))
        while row:
            record = ""
            video_id = str(row[0]).rstrip()
            prog_id = str(row[1]).rstrip()
            episode = int(row[2])
            prog_name = str(row[3]).rstrip()
            play_date = str(row[4]).rstrip()
            play_time = str(row[5]).rstrip()
            queue = [video_id, prog_id, episode, prog_name, play_date, play_time]
            if (len(video_id) > 0 and video_id != 'None' and video_id not in video_id_dict):  # use regular expression instead
                video_id_dict[video_id[2:]] = queue
            playlist.append(queue)
            """
            for i in range(len(row)):
                record = record + str(row[i]).rstrip() + "|"
            print(record)
            """
            count = count + 1
            row = cursor.fetchone()

    for queue in playlist:
        print(queue)
    print("Total count : " + str(count))

    not_found = []
    for vid in video_id_dict.keys():
        print(vid + ":" + str(video_id_dict[vid]))
        target_file = APPROVED_DIR + "\\" + vid + ".mxf"
        if (os.path.isfile(target_file)):
            print ("Found " + target_file)
        else:
            print ("Cannot find " + target_file)
            not_found.append(vid)

    print("\n\n\n\n--------------------------- File not found ---------------------------------")
    for vid in not_found:
        print(str(video_id_dict[vid]) + " not found")

#ptsutils3.create_database("audio_test.db3")

#get_playlist_prog()
#video_id_dict = ptsutils3.get_playlist_all(str_date, str_channel)

#move  testing files to  ATS_IN
SRC_DIR = r'\\10.13.200.22\mamnas1\Approved'
TARGET_DIR = r'\\10.13.200.22\mamnas1\ATS_IN'
#ptsutils3.copy_file_by_video_id(video_id_dict, SRC_DIR, TARGET_DIR)
#ptsutils3.move_file_by_video_id(video_id_dict, SRC_DIR, TARGET_DIR)

SRC_DIR = r'\\10.13.200.22\mamnas1\Approved'
TARGET_DIR = r'\\10.13.200.22\mamnas1\Approved'
#ptsutils3.rename_video_id(video_id_dict, SRC_DIR, TARGET_DIR)

SRC_DIR = r'\\10.13.200.22\mamnas1\Approved\ATS_IN'
TARGET_DIR = r'\\10.13.200.22\mamnas1\Approved'
#comp_approved_atsin(video_id_dict, SRC_DIR, TARGET_DIR)

#convert louth lst file
#ptsutils3.modify_louth_lst("1320180130(louth).lst")
#ptsutils3.modify_louth_lst("1320180201(louth).lst")
ptsutils3.modify_louth_lst("1320180131(louth).lst")

"""
for vid in video_id_dict.keys():
    print(vid[3:])
    f =  r'"\\10.13.200.6\MAMDownload\AUDIO_TEST\ATS_IN\0046MA.mxf"' #是目前用來啟動 CMD.EXE 的目錄路徑。不支援 UNC 路徑
    f = r'U:\AUDIO_TEST\ATS_IN\' + vid + r'.mxf'
    cmd = "powershell.exe -noexit '& new-item -verbose -itemtype file -path " + f
    os.system(cmd)
"""
