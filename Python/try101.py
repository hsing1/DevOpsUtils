# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 21:43:44 2017

@author: hsing1
"""

import os
import subprocess
import sys
import itertools
import re
import shelve
import sqlite3
import datetime
import time
import ptsutils3
import config
from ftplib import FTP
import threading
import getopt


print(len(sys.argv))
print(sys.argv[0])

"""
To activate virtual env : .\myenv\Scripts\activate
To launch python for Django : python .\mysite\manage.py shell
To get help : python .\Django\mysite\manage.py help

elif (case == 'django_template'):
    from django import template
    t = template.Tamplate('My name is {{ name }}.')

    c = template.Context({'name' : 'Nige'})
    print (t.render(c))

    t = Template('Hello, {{ name }}')
    for name in ('John', 'Juli', 'Pat'):
        print (t.render(Context({'name' : name}))

    #access to object attribute
    from django.template import Template, Context
    import datetime
    d = datetime.date(2017, 5, 2)
    t = Template('The mopnth is {{ date.month }} and the year is {{ date.year }}.')
    c = Context({'date' : d})
    t.render(c)


    #use custom class
    class Person(object):
        def __init__(self, first_name, last_name):
	    self.first_name, self.last_name = first_name, last_name
    t = Template('Hello, {{ person.first_name }} {{ person.last_name }}.')
    c = Context({'person' : Person('John', 'Smith')})
    t.render(c)

    #refer to method of objects
    t = Template('{{ var }} -- {{ var.upper }} -- {{ var.isdigit }}')
    t.render(Context({'var' : 'hello'}))
    t.render(Context({'var' : '123'}))

    #access to list index
    t = Template('Item 2 is {{ item.2 }}.')
    c = Context({'items' : ['apples', 'bananas', 'carrots']})
    t.render(c)
"""
#Modified on 24.17
#Modified on missing-link
case = sys.argv[1]
print ("case = {}".format(case))
#add comment @pts.hsing1
if (case == 'C_email'):
    if (1):
        is_valid = False
        from validate_email import validate_email
        #is_valid = validate_email('----@gmail.com')
        #is_valid = validate_email('ai0975@ms.ntpc.gov.tw', verify=True) #None
        is_valid = validate_email('esing9063@yahoo.com', verify=True) #True
        print(is_valid)
        
elif (case == "C_subprocess"):
    hsm_login = config.HSM_CONFIG.login
    hsm_passwd =  config.HSM_CONFIG.password
    tsm_login = config.TSM_CONFIG.tsm_user1[0] 
    tsm_passwd = config.TSM_CONFIG.tsm_user1[1]
    hsm = config.HSM_CONFIG.HSM['HSM4']

    if (0):
        plink_args = ['plink', '-ssh', hsm, '-l', hsm_login, '-pw', hsm_passwd]
        dsmadmc_args = ['dsmadmc', '-id=' + tsm_login, '-password=' + tsm_passwd]
        plink_args.extend(dsmadmc_args)
        query_args = ['query', 'vol', 'STGpool=PTSTVHSM']
        plink_args.extend(query_args)
        proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
        lines = proc.stdout.readlines()
        rc = proc.returncode
        exit_code = proc.wait()
        for l in lines:
            print(l)

    if (1):
        plink_args = 'plink -ssh {hsm} -l {login} -pw {passwd}'.format(hsm = hsm, login = hsm_login, passwd = hsm_passwd)
        dsmadmc_args = 'dsmadmc -id={login} -password={passwd}'.format(login = tsm_login, passwd = tsm_passwd)
        query_args = 'query vol STGPool={pool}'.format(pool = 'PTSTVHSM')
        cmd  = '{plink} {dsmadmc} {query}'.format(plink = plink_args, dsmadmc = dsmadmc_args, query = query_args)
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        lines = proc.stdout.readlines()
        rc = proc.returncode
        exit_code = proc.wait()
        for l in lines:
            print(l)


elif (case == 'C_string'):
    if (1):
        bs = b'00339F'
        print(bs)
        print(bs[0])
        ss = bs.decode("utf-8")
        print(ss)
        print(ss[0])
        ss = 'T' + ss[1:]
        bbs = ss.encode("utf-8")
        print(bbs)
    if (0):
        s = '002K3B'
        #s[0] = 'T'
        print(s[0])
        print(s[1:])
        s = 'T' + s[1:]
        print(s)
    if (0): #concat string
        s = "123456789" \
            "abcdefg"
        print(s)
        s = "This is the first line of my text, " \
            "which will be joined to a second."
        print("1:{}".format(s))
        s = "This is the first line of my text, " + "which will be joined to a second."
        print("2:{}".format(s))
        s = ("This is the first line of my text, " +
             "which will be joined to a second.")
        print("3:{}".format(s))
        s = ("This is the first line of my text, ",
             "which will be joined to a second.")
        print("4:{}".format(s))
        s = """This is the first line of my text,
             which will be joined to a second."""
        print("5:{}".format(s))
        s = """This is the first line of my text, \
             which will be joined to a second."""
        print("6:{}".format(s))
        vid = '0046MR'
        #s = r"C:\AUDIO_TEST\ATS_IN\\" + vid + r".mxf"
        s = r"C:\temp\\" + vid + r".mxf"
        #s = r'U:\AUDIO_TEST\ATS_IN + vid
        print("7:{}".format(s))
        command = 'powershell.exe -noexit "& new-item -path" ' + s + " -itemtype file"
        os.system(command)
    if (0):
        x = "Item One : {} Item Two {}".format("dog", "cat")
        print(x)
        x = "Item One : {x} Item Two {y}{y}".format(x = "dog", y = "cat")
        print(x)
        x = 'Item One : {x} Item Two {y}{y}'.format(x = "dog", y = "cat")
        print(x)
    if (0):
        s = "Django"
        print (s[::-1]) #print string reversely

elif case == "C_ftp":
    ftp = FTP()
    ftp.connect("10.232.1.74")
    ftp.encoding = "UTF-8"
    ftp.login("mxfmovie", "mxfmovie")
    ftp.cwd("ONAIR")
    ftp.dir()

elif case == "C_thread":
    pass

elif case == "C_file_size":
    if 1:
        MAMDownload = "\\\\10.13.200.22\\mamnas1\\Approved\\"
        video_id = '002SJR'
        path = MAMDownload + video_id + ".mxf"
        size = ptsutils3.get_file_size(path)
        print(size)

elif case == "C_enum":
    if 1:
        from enum import Enum

        class Status(Enum):
            RECALL_OK = 0
            RECALL_FAIL = 1
            
        if (Status.RECALL_OK):
            print(Status.RECALL_OK)
        
        if (Status.RECALL_FAIL):
            print(Status.RECALL_FAIL)

elif case == "C_excel":
    if 1:
        from openpyxl import Workbook
        wb = Workbook()

        # grab the active worksheet
        ws = wb.active

        # Data can be assigned directly to cells
        ws['A1'] = 42

        # Rows can also be appended
        ws.append([1, 2, 3])

        # Python types will automatically be converted
        import datetime
        ws['A2'] = datetime.datetime.now()

        # Save the file
        wb.save("sample.xlsx")

    if 0: #fail
        import openpyxl as px
        import numpy as np

        file_name = "c:\temp\toapprove.xlsx"
        W = px.load_workbook(file_name, use_iterators = True)
        p = W.get_sheet_by_name(name = 'Sheet1')

        a=[]

        for row in p.iter_rows():
            for k in row:
                a.append(k.internal_value)
        """
        # convert list a to matrix (for example 5*6)
        aa= np.resize(a, [5, 6])

        # save matrix aa as xlsx file
        WW=px.Workbook()
        pp=WW.get_active_sheet()
        pp.title='NEW_DATA'

        f={'A':0,'B':1,'C':2,'D':3,'E':4,'F':5}

        #insert values in six columns
        for (i,j) in f.items():
            for k in np.arange(1,len(aa)+1):
                pp.cell('%s%d'%(i,k)).value=aa[k-1][j]

        WW.save('newfilname.xlsx')
        elif case == "byte":
            if (1):
                x = b'\xd8\xb7'
                print(x)
                y = x.decode('cp855')
                print(y)
                x = '10ff'
                y = bytes.fromhex(x)
                print(y)
                x = ord(b'm')
                print(x)
                x = ord('m')
                print(x)
                x = 'Python bytes'
                z = list(x)
                print(z)
                x = b'Python bytes'
                z = list(x)
                print(z)
        """

elif case == "C_TSM":
    if (1):
        ptsutils3.get_libvol("TS3500")

elif case == "reading_byte":
    if (1):
        fname = "1320180130(louth).lst"
        out = "new.lst"
        CHUNK_SIZE = 104
        fh = open(fname, "rb")
        fo = open(out, "wb")
        bytes_read = fh.read(CHUNK_SIZE)
        newbyte_array = bytes_read[0:16] + bytes_read[16:25] + bytes_read[25:104]
        fo.write(newbyte_array)

    if (0):
        fname = "1320180130(louth).lst"
        out = "new.lst"
        CHUNK_SIZE = 104
        fh = open(fname, "rb")
        fo = open(out, "wb")
        bytes_read = fh.read(CHUNK_SIZE)
        video_id = bytes_read[16:24]
        str_video_id = video_id.decode("utf-8")
        str_video_id = 'T' + str_video_id[1:]
        video_id = str_video_id.encode("utf-8")
        newbyte_array = bytes_read[0:16] + video_id + bytes_read[25:103]
        fo.write(newbyte_array)

    if (0):
        fname = "1320180130(louth).lst"
        out = "new.lst"
        CHUNK_SIZE = 104
        fh = open(fname, "rb")
        fo = open(out, "wb")
        bytes_read = fh.read(CHUNK_SIZE)
        video_id = bytes_read[16:24]
        fo.write(bytes_read)

    if (0):
        fname = "1320180130(louth).lst"
        out = "new.lst"
        CHUNK_SIZE = 104
        fh = open(fname, "rb")
        bytes_read = fh.read(CHUNK_SIZE)
        video_id = bytes_read[16:24]
        print(str(video_id))
        for b in video_id:
            print(str(b))
        print("\n")

    if (0):
        fname = "1320180130(louth).lst"
        out = "new.lst"
        CHUNK_SIZE = 104
        fh = open(fname, "rb")
        bytes_read = fh.read(CHUNK_SIZE)
        while bytes_read:
            video_id = bytes_read[16:24]
            print(str(video_id))
            for b in video_id:
                print(str(b))
            print("\n")
            bytes_read = fh.read(CHUNK_SIZE)



elif case == "fileop":
    if (1):
        os.rename("ATS_IN\\t", ".\\t")
        os.rename("t", "ATS_IN\\t")

elif case == "sqlite3":
    if (1):
        conn = sqlite3.connect("test.db")
        tsql = '''CREATE TABLE AUDIO_TEST (VIDEO_ID text, FCSTATUS text, FDDATE text)'''
        #ptsutils3.exec_tsql(conn, tsql)
        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        print(st)
        tsql = 'INSERT INTO AUDIO_TEST VALUES(\'004E33\', \'A\', \'' + st + '\')'
        print(tsql)
        ptsutils3.exec_tsql(conn, tsql)

        tsql = '''SELECT * from AUDIO_TEST'''
        ptsutils3.exec_select(conn, tsql)


    if (0):
        conn = sqlite3.connect("example1.db")
        print(sqlite3.version)
        print(sqlite3.version_info)
        c = conn.cursor()
        c.execute('''CREATE TABLE stocks (date text, trans text, symbol text, qty real, price real)''')
        c.execute("INSERT INTO stocks VALUES ('2006-01-05','BUY','RHAT',100,35.14)")
        conn.commit()
        conn.close()

    # Never do this -- insecure!
    if (0):
        conn = sqlite3.connect('example1.db')
        c = conn.cursor()
        symbol = 'RHAT'
        c.execute("SELECT * FROM stocks WHERE symbol = '%s'" % symbol)
        print(c.fetchone())

    # Do this instead
    if (0):
        conn = sqlite3.connect('example.db')
        c = conn.cursor()
        t = ('RHAT',)
        c.execute('SELECT * FROM stocks WHERE symbol=?', t)
        print(c.fetchone())

        conn.close()



elif case == "audio_test":
    if (0):
        for r, ds, fs in os.walk("./"):
            print ("root = " + r)
            print ("loop files")
            for f in fs:
               print (os.path.join(r, f))

            print ("loop dirs")
            for d in ds:
               print ("DIR = " + os.path.join(r, d))
    if (1):
        target_dir = r"\\10.13.200.6\MAMDownload\AUDIO_TEST\ATS_IN"
        inf = r'\\10.13.200.25\MAMDownload\hsing1\repository\DevOpsUtilsTest\Python\video.txt'
        fh = open(inf, "r")
        lines = fh.readlines()
        for l in lines:
            video_id = l.rstrip()
            if (video_id == "NULL" or video_id == ''):
                continue
            target = target_dir + '\\' + video_id + ".mxf"
            print(target)
            if (os.path.isfile(target)):
                print("Found " + target)
            else:
                print("Cannot find " + target)


elif case == "join":
    if 1:
        print( os.path.join("/mnt", "a.txt")) #/mnt/a.txt
elif (case == "scan"):
   TARGET_DIR = "/mnt/HDUpload_old/STGPOOL_VOL"
elif case == "TSM":
    pass
elif (case == "re"):
    if (True):
        fPath = ["/ptstv/HVideo/2017/20170900768/P201709007680001.mxf"
        , "/ptstv/.SpaceMan/logdir/translog5/0A0DD20C4DCBA50C000000000000947F00000000000000000001000200000000.rec"
        , "/ptstv/HVideo/2017/20170900768/P201709007680001.MXF"
        ]
        extObj = re.compile(".*\.mxf$", re.IGNORECASE)

        for f in fPath:
            result = extObj.match(f)
            if (result != None):
                print(result.group(0))
            else:
                print(result)

elif (case == "open"):
    fh = open("remeo.txt", "r")

    count = 0
    for line in fh:
        print(line.strip())
        count = count + 1

    #print(count, "Lines") ==> Python 3
    print(count)

elif (case == "for"):
    if (False):
        list = [['1', '2', '3'], ['a', 'b', 'c'], ['D', 'B', 'C']]
        for i, j, k in list:
            print(i)
            print(j)
            print(k)

    if (False):
        tuples = [1,2,3], [4,5,6], [7,8,9]
        for i, j, k in tuples:
            print(i)
            print(j)
            print(k)

    if (1):
        tuples = 1,2,3
        for i, j, k in tuples: #TypeError: unpack non-sequence
            print(i)
            print(j)
            print(k)


    if (0):
        tuples = [(1,2,3, 4), (4,5,6)] #ValueError: too many values to unpack
        for i, j, k in tuples:
            print (i)
            print (j)
            print (k)

    if (False):
        ds = [('./', ['B'], ['a.txt', 'b.txt']), ('./B', [], ['bb.txt', 'aa.txt'])]
        print (ds)
        for root, dirs, files in ds:
            print (root)
            for f in files:
                print (os.path.join(root, f))

elif (case == "walk"):
    if (1):
        search = ['201310270010006', '20140800088']
        vol_dir = '\\\\10.13.200.30\\GPFS\VOL'
        tape_info = ptsutils3.get_tape_info_by_fileno(search, vol_dir)
        print(tape_info)
    if (0):
        VOL_DIR = '\\\\10.13.200.30\\GPFS\\VOL'
        count = 1
        found = False
        for r, ds, fs in os.walk(VOL_DIR):
            for f in fs:
                count += 1       
                if (count > 10000):
                    break
                if ("_" in f):
                    continue

                file_name = VOL_DIR + '\\' + f
                #print("check " + file_name)
                fh = open(file_name, "r")
                lines = fh.readlines()
                target = '201310270010006'  #reside in K00030L4
                for l in lines:
                    if (target in l):
                        print(f + ":" + l)
                        found = True
                        break
                fh.close()
                if (found):
                    break
    if (0):
        VOL_DIR = '\\\\10.13.200.30\\GPFS\\VOL'
        count = 1
        for r, ds, fs in os.walk(VOL_DIR):
            for f in fs:
                count += 1
                print(f)
                print("\n")
                if (count > 100):
                    break
    if (0):
        p = r'\\10.13.200.5\MAMUpload\AUDIO_TEST\ATS_IN'
        count = 1
        for r, ds, fs in os.walk(p):
            print ("root" + str(count) + " = " + r)
            print ("loop files")
            for f in fs:
               print (os.path.join(r, f))

            print ("loop dirs")
            for d in ds:
               print ("DIR = " + os.path.join(r, d))
            count += 1
    if (0):
        p = r'\\10.13.200.5\MAMUpload\AUDIO_TEST\ATS_IN'
        for response in os.walk(p):
            print(response)

elif (case == "listdir"):
    def doInDir(someDir):
        print (someDir)
        fileList = os.listdir(someDir)
        for f in fileList:
            fullPath = os.path.join(someDir, f)
            if os.path.isdir(fullPath):
               doInDir(fullPath)
            elif os.path.isfile(fullPath):
               print (fullPath)

    p = r'\\10.13.200.5\MAMUpload\AUDIO_TEST\ATS_IN'
    doInDir(p)

elif (case == "glob"):
    if (True):
        p = "*.py"
        p = "/Media/Photo/ptstv/2015/2015230/G2015230*.jp*g"
        fList = glob.glob(p)
        for f in fList:
            print(f)
elif (case == "split"):
    if 1:
        fh = open("gpfs2approve.txt", "r")
        lines = fh.readlines()
        for l in lines:
            l = l.rstrip()
            print(l)
            #arr = l.split(r'\t')
            arr = re.split('\s+', l)
            print(arr)
    if 0:
        s = "a   b c     e"
        str2 = re.split("( )+", s) #['a', ' ', 'b', ' ', 'c', ' ', 'e']
        print(str2)
        str2 = re.split(" +", s)
        print(str2)
        str2 = re.split("\s+", s)
        print(str2)
        s = r"003PLX	\\mamstream\MAMDFS\ptstv\HVideo\2017\2017756\G201775600210002.mxf"
        print(s)
        str2 = re.split("\s+", s) #work
        print(str2)
        s = r"003PLX	\\mamstream\MAMDFS\ptstv\HVideo\2017\2017756\G201775600210002.mxf"
        str2 = s.split("\s+") #fail
        print(str2)

elif case == "TEST":
    str = 'TEST'
    #os.system('mkdir ' +  str)
    proc = subprocess.Popen(['ls'], stdout=subprocess.PIPE)
    print (proc.stdout.readlines())

    str = "a    b    c d"
    arr = str.split()
    print (arr)

    for root, dirs, files in os.walk("/root/bin", topdown=False):
        for name in files:
            print(root + '--' + name)
            print(os.path.join(root, name))

        for name in dirs:
            print(root + ' ' + name)
            print(os.path.join(root, name))

elif case == "package":
    if 1:
        from  pack.my_module import my_func
        my_func()
    if 0:
        import pack.my_module as p
        p.my_func()
    if 0:
        import pack.my_module
        pack.my_module.my_func()
    if 0:
        from my_module import my_func
        my_func()
    if 0:
        import my_module
        my_module.my_func()

elif (case == "variable"):
    if 1:
        def my_func():
            x = 10
            print(locals())
            print(locals()['x'])
        my_func()

        print(globals())


elif (case == 'class'):
    if (1):
        class Book():
            def __init__(self, title, author, pages):
                self.title = title
                self.author = author
                self.pages = pages

            def __str__(self):
                return "Title: {}, Author: {}, Pages: {}".format(self.title, self.author, self.pages)

            def __del__(self):
                print("A book is delete")

        b = Book("Python", "jose", 200)
        print(b)

    if (0):
        class Animal():
            def __init__(self):
                print("ANIMAL CREATED")

            def whoAmI(self):
                print("ANIMAL")

            def eat(self):
                print("EATING")

        class Dog(Animal):

            def __init__(self):
                Animal.__init__(self)
                print("DOG CREATED")

            def bark(self):
                print("WOOF")

            def eat(self):
                print("DOG EATING")


    if (0):
        class Circle():
            pi = 3.14

            def __init__(self, radius = 1):
                self.radius = radius

            def area(self):
                return self.radius * self.radius * Circle.pi

            def set_radius(self, radius):
                self.radius = radius

        myc = Circle(3)
        print(myc.radius)
        print(myc.area)
        print(myc.area())
        myc.radius = 100
        print(myc.area())
        myc.set_radius(200)
        print(myc.area())

    if (0):
        class Dog():

            # CLASS OBJECT ATTRIBUTE
            species = "mammal"

            def __init__(self, breed, name):
                self.breed = breed
                self.name = name

        mydog = Dog(name = "Lucky", breed = "Lab")
        otherDog = Dog("Huskie", "John")

        print(mydog.breed)
        print(mydog.name)

    if (0):
        class Sample():
            pass
        x = Sample()
        print(type(x))

elif (case == 'list'):
    if (1):
        mylist = ['stinnn', 1, 2, True, [1,2,3]]
        print (mylist)
        mylist.append(['a', 'b', 1]) # appends object at end
        print (mylist)
        mylist.extend(['a', 'b', 1]) #extend list by appending elements from the iterable
        print (mylist)
        item = mylist.pop() #pop from last one
        print (mylist)
        print (item)
        item = mylist.pop(0) #pop from 1st index
        print (mylist)
        print (item)
        matrix = [[1,2,3], [4,5,6], [7,8,9]]
        first_col = [row[0] for row in matrix]
        print (first_row)
    if (0): #list comprehensions
        S = [x**2 for x in range(10)]
        print (S)
        M = [x for x in S if x % 2 == 0]
        print (M)
        words = 'The quick brown fox jumps over the lazy dog'.split()
        print(words)
        stuff = [[w.upper(), w.lower(), len(w)] for w in words]
        for i in stuff:
            print (i)
elif (case == "dictionary"):
   if (1):
      my_stuff = {"key" : 123, "key2" : "value2", 'key3' : {'123' : [1,2, 'grab me']}}
      print (my_stuff['key3']['123'][2])
      my_stuff['key4'] = "value4"
      print (my_stuff)

elif (case == 'tuple'):
   if (1):
      t = (1,2,3)
      print (t)
      print (t[0])
elif (case == "set"):
   if (1):
       x = set()
       x.add(1)
       x.add(2)
       x.add(3)
       print (x)
       x = set([1,1,1,2,2,3,3,4,4])
       print (x)

       mylist = [1,1,1,2,2,2,3,3,3,]
       x = set(mylist)
       print(x)
elif (case == "sqlserver"):
    if (1):
       import pyodbc
       server = '10.13.220.35'
       database = 'MAM'
       username = 'hsing1'
       password = '0123456789'
       connStr = 'DRIVER={{ODBC Driver 13 for SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s= server, d = database, u = username, p = password)
       cursor = cnxn.cursor()
       tsql = "select * from TBUSERS where FSUESR = 'hsing1';"
       with cursor.execute(tsql):
           row = cursor.fetchone()
           while row:
               print(str(row[0], " " + str(row[1])))
               row = cursor.fetchone()
elif (case == "for"):
    if (1):
        mypairs = [(1,2), (3,4), (5, 6)]
        for (tup1, tup2) in mypairs:
            print (tup1)
            print (tup2)


elif (case == "function"):
    if 1:
        def sum(*numbers):
            total = 0
            for number in numbers:
                total += number
            return total
        print(sum(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
    if (0):
        def end_other(a, b):
            a = a.lower()
            b = b.lower()

            #return (b.endswith(a) or a.endswith(b))
            return a[-(len(b)):] == b or a == b[-len(a):]
        print (end_other("abcd", "cd"))
        mystr = "abcd"
        print (mystr[-5:])

    if (0):
        str1 = "0123456789"
        def stringBits(mystr):
            result = ""
            for i in range(len(mystr)):
                if i%2==0:
                    result = result + mystr[i]
            return result

        print(stringBits("0123456789"))
        print(str1[::2])
        print(str1[::3])

    if (0):
        def my_func():
            """
            THIS IS THE DOCSTRING
            """
            print("my function")

        my_func()
    if (0):
        def arrayCheck(nums):
            for i in range(len(nums) - 2):
                if nums[i] == 1 and nums[i+1] == 2 and nums[i+2] == 3:
                    return True
            return False
        print (arrayCheck([0,1,2,3,4,5,6]))
        print (arrayCheck([0,1,3,4,5,6]))
        print (arrayCheck([0,0,1,2,3,4,5,6]))

elif (case == "lambda"):

    if 1:
        min = lambda a, b: a if a < b else b
        minimum = min
        print(min(10, 20))
        print(minimum(10, 20))

    if (0):
        mylist = [1,2,3,4,5,6,7,8]

        def even_bool(num):
            return num%2 == 0

        evens = filter(even_bool, mylist)
        print (list(evens))

        evens2 = filter(lambda num:num%2==0, mylist)
        print (list(evens2))
elif (case == 'shelve'):
    if (False):
        dBase = shelve.open("myDbase")
        object1 = ['The', 'bright', ('side', 'of'), ['life']]
        object2 = {'name' : 'Brian', 'age' : 33, 'motto' : object1}
        dBase['brian'] = object2
        dBase['knight'] = {'name' : 'Knight', 'motto' : 'Nil'}
        print(len(dBase))

    if (True):
        dBase = shelve.open("myDbase")
        print(dBase.keys())
        for row in dBase.keys():
            print(row, "=>")
            for field in dBase[row].keys():
                print (fields, "=>", dBase[row][field])

elif (case == 'list'):
    book = ['Python', 'Development', 8]
    book.append(2008)
    print(book)
    book.insert(1, 'Web')
    print(book)
    print(book[:3])
    print('Django' in book)
    book.remove(8)
    print(book)
    print(book.pop(-1))
    print(book)
    print(book * 2)
    print(book)
    print(book.extend(['with', 'Django'])) #None
    print(book)

    print(book.sort()) #None
    print(book)

else:
    print ("nothng")


def thread_job():
    # 把目前的 thread 顯示出來看看
    print("Begin thread, {}\n".format(threading.current_thread()))
    time.sleep(30)
    print("End thread {}\n".format(threading.current_thread()))

def main(argv):

    try:
        opts, args = getopt.getopt(argv, "", ["case="])

    except Exception as e:
        template = "Exception {0} occur : {1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)

        sys.exit(2)
    
    for opt, arg in opts:
        print("Process case %s" %opt)
        if (opt == "--case"):
            case = "C_" + arg

        if (case == "C_threading"):
            if (0):
                # 添加一個 thread 
                added_thread = threading.Thread(target=thread_job)
                # 執行 thread
                added_thread.start() # This is an added Thread, number is <Thread(Thread-1, started 123145466363904)>
                # 看目前有幾個 thread
                print(threading.active_count()) # 2
                # 把所有的 thread 顯示出來看看
                print(threading.enumerate()) # [<_MainThread(MainThread, started 140736627270592)>, <Thread(Thread-1, started 123145466363904)>]
                # 把目前的 thread 顯示出來看看
                print(threading.current_thread()) #<_MainThread(MainThread, started 140736627270592)>

            if (1):
                for n in range(100):
                    added_thread = threading.Thread(target=thread_job)
                    added_thread.start()

                    thread_count = threading.active_count()
                    if (thread_count > 5):
                        print("Seleeping ...")
                        time.sleep(10)

                    #added_thread.join()
            #end of C_threading

        elif (case == 'C_regex'):
            if 0:
                s = '00463X	\\mamstream\MAMDFS\hakkatv\HVideo\2017\20171200720\P201712007200001.mxf jjlllkjklj'
                unc_path_pat = '^(?:[a-z]:|\\\\[a-z0-9_.$●-]+\\[a-z0-9_.$●-]+)\\(?:[^\\/:*?"<>|\r\n]+\\)*[^\\/:*?"<>|\r\n]*$'
                unc_path_pat = r'.+\\\\[a-zA-Z0-9\.\-_]{1,}(\\[a-zA-Z0-9\-_]{1,}){1,}[\$]{0,1}.+'
                reObj = re.compile(unc_path_pat)
                res = reObj.match(s)
                if (res != None):
                    print(res.group(0))


            if 0:
                event1 = '呼叫TSM-ReCall,JOBID=288727	2011263	UPLOAD	G201126300010001	003G43	\\mamstream\MAMDFS\hakkatv\HVideo\2011\2011263\G201126300010001.mxf	\\10.13.200.22\mamnas1\Approved\003G43.mxf	2018-02-21 05:00:10:00'
                event2 = '磁帶不在架上	2016546	UPLOAD	G201654600080004	002UL6	\\mamstream\MAMDFS\hakkatv\HVideo\2016\2016546\G201654600080004.mxf	\\10.13.200.22\mamnas1\Approved\002UL6.mxf	2018-02-21 07:55:47:15'
                event3 = '正在上傳	2013682	UPLOAD	G201368200010003	0026SD	\\mamstream\MAMDFS\ptstv\HVideo\2013\2013682\G201368200010003.mxf	\\10.13.200.22\mamnas1\Approved\0026SD.mxf	2018-02-26 16:20:00;00'
                event4 = '可以開始複製	2009299	UPLOAD	G200929908260002	00475X	\\mamstream\MAMDFS\hakkatv\HVideo\2009\2009299\G200929908260002.mxf	\\10.13.200.22\mamnas1\Approved\00475X.mxf	2018-02-26 18:30:00;00'
                event5 = '呼叫TSM-ReCall,JOBID=288727 2011263 UPLOAD G201126300010001 003G43 \\mamstream\MAMDFS\hakkatv\HVideo\2011\2011263\G201126300010001.mxf \\10.13.200.22\mamnas1\Approved\003G43.mxf 2018-02-21 05:00:10:00'
                event6 = """呼叫TSM-ReCall,JOBID=288727 2011263 UPLOAD G201126300010001 003G43 \\mamstream\MAMDFS\hakkatv\HVideo\2011\2011263\G201126300010001.mxf \\10.13.200.22\mamnas1\Approved\003G43.mxf 2018-02-21 05:00:10:00"""
                event7 = '7	23	UPLOAD	G201126300010001	003G43	\\mamstream\MAMDFS\hakkatv\HVideo\\2011\\2011263\G201126300010001.mxf	\\10.13.200.22\mamnas1\Approved\\003G43.mxf	2018-02-21 05:00:10:00'

                pattern1 = r'.+TSM-ReCall.+'
                pattern2 = r'磁帶不在架上'
                pattern3 = r'正在上傳'
                pattern4 = r'可以開始複製'
                pattern5 = r'.+?TSM-ReCall.+?'  #non greedy matching
                video_id_pat = r'.+?\t?00\w{4}\t+.+?'

                reObj = re.compile(pattern1)
                res = reObj.match(event1)
                print("case1")
                print(res)
                print(res.group(0))

                print("case2")
                reObj = re.compile(pattern1)
                res = reObj.match(event6)
                print(res)
                print(res.group(0))

                print("case3")
                reObj = re.compile(r'.+?(\w{6})\.mxf.+?')
                res = reObj.match(event7)
                print(res)
                print(res.group(0))

                print("case4")
                reObj = re.compile(video_id_pat)
                res = reObj.match(event1)
                print(res)
                print(event1)

            if 0:
                path = r'\\10.13.200.22\mamnas1\Approved\00474Z.mxf'
                path = '\\10.13.200.22\mamnas1\Approved\AAAAAZ.mxf'
                path = 'AAAAAA.mxf'
                reObj = re.compile('\w{6}.mxf') # no match
                reObj = re.compile('\w{6}\.mxf') # match
                reObj = re.compile('\\w{6}\.mxf') # no match
                reObj = re.compile(r'\w{6}.mxf') # match
                res = reObj.match(path)
                print(res)

                path = '\\AAAAAAZ.mxf'
                reObj = re.compile('\\\\\w{6}Z\.mxf') #match
                res = reObj.match(path)
                print(res)

                path = r'\\AAAAAAZ\.mxf'
                reObj = re.compile('\w{6}Z\.mxf') #not match
                reObj = re.compile('\\\\AAAAAAZ\.mxf') #not #match
                res = reObj.match(path)
                print(res)

            if 0:
                video_id = "E00018L4"
                reObj = re.compile(r'[A-Z]{1}\d{5}L4')
                res = reObj.match(video_id)
                print(res)

            if 0:
                p = '\\mamstream\MAMDFS\hakkatv\HVideo\2017\2017833\G201783300060001.mxf'
                reObj = re.compile(r'.mamstream.', re.IGNORECASE)
                res = reObj.match(p)
                print(res)
                print(res.group(0))

            if (0):
                print(re.findall('match', 'test phrase match in match middle'))

                def multi_re_find(patterns, phrase):

                    for pat in patterns:
                        print("searching for pattern {}".format(pat))
                        print(re.findall(pat, phrase))
                        print("\n")

                test_phrase = 'sdsdfsdfsdfd...sdfsd...ddd.....ssddddsssddd123'
                test_patterns = ['sd+', 'sd?', 'sd*', 'sd{1,3}', '[a-z]+', r'\d+', r'\s+', r'\w+']

                print(multi_re_find(test_patterns, test_phrase))

            if (0):
                text = 'This is a string with term1, but not the other!'
                match = re.search('term1', text)
                print(type(match)) #<class '_sre.SRE_Match'>
                print(match.start())

                email = 'user@gmail.com'
                split_term = '@'
                print(re.split(split_term, email))
            if (0):
                patterns = ['term1', 'term2']

                text = 'This is a string with term1, but not the other!'

                for pattern in patterns:
                    print("I'm searching for:" + pattern)

                    if re.search(pattern, text):
                        print("MATCH!")
                    else:
                        print("NO MATCH!")

            #end of C_regex

        elif (case == "C_ldap"):
            import ldap3
            if (1):
                from ldap3 import Server, Connection, ALL, NTLM
                server = Server('pts.org.tw', get_info=ALL)
                conn = Connection(server, user='PTS\\mamadmin', password='mamadmin', authentication=NTLM)
                conn.bind()
                result = conn.search('ou=客家電視台,ou=hr,dc=pts,dc=org,dc=tw', '(objectclass=person)')
                print(result) #True
                result = conn.search('ou=台語頻道籌備中心,ou=hr,dc=pts,dc=org,dc=tw', '(objectclass=person)')
                print(result) # True
                print(conn.entries)

            if (0):
                from ldap3 import Server, Connection, ALL, NTLM
                server = Server('pts.org.tw', get_info=ALL)
                #server = Server('pts.org.tw')
                conn = Connection(server, user='PTS\\mamadmin', password='mamadmin', authentication=NTLM)
                #conn = Connection(server, user='mamadmin', password='mamadmin')
                conn.bind()
                w = conn.extend.standard.who_am_i()
                print(w)
                #print(conn)
            if (0):
                server = ldap3.Server('pts.org.tw')
                conn = ldap3.Connection(server)
                conn.bind()
                print(conn)


if (__name__ == "__main__"):
    main(sys.argv[1:])