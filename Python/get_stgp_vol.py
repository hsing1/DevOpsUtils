# -*- coding: UTF-8 -*-

import os
import config
import subprocess
import ptsutils3
import sys

#TARGET_DIR = "/mnt/MAM_ARCHIVE/TSM_INFO/STGPOOL_VOL"
TARGET_DIR = "\\\\isilon.pts.org.tw\\MAM_ARCHIVE\\TSM_INFO\\STGPOOL_VOL"
stgPoolList = ['NEWSTITV',
               'FILE',
               'MEDIA',
               'HAKKATV',
               'MVTV',
               'NEWSHAKKA',
               'NEWSPTS',
               'NEWSTITV',
               'PTSTV',
               'TITV',]

poolType = ['HSM', 'HSMCP', 'BAK', 'BAKCP']

def main(argv):
    for pool in stgPoolList:
        for type in poolType:
            stgp = pool + type
            outputF = TARGET_DIR + "/" + stgp + ".txt"
            ptsutils3.dump_stgp_vol_info(stgp, outputF)
            size = check_file(stgp, outputF)
            msg = "query {p} ({s})".format(p = stgp, s = str(size))
            print(msg)


def check_file(stgp, outf):
    size = 0
    while (True):
        size = ptsutils3.get_file_size(outf)
        if (size < 100): # regenerate file
            msg = "uncomplete file, regenerate stgpool {p}".format(p = stgp)
            print(msg)
            ptsutils3.dump_stgp_vol_info(stgp, outf)
        else:
            break
    return size


if (__name__ == "__main__"):
    main(sys.argv[1:])