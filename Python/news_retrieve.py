# -*- coding: UTF-8 -*-

import os
import sys
import pyodbc
import config
import shutil
import getopt
import time
import re
from datetime import datetime
from check_file_status_win import check_by_file_no, check_by_file_list
from check_file_status_win import check_by_file_list_1, update_off_site_vol_list
from mam_archive import get_prog_archive_info
from mam_retrieve import dsmls, dsmrecall
import subprocess
import ptsutils3
import smtplib

TARGET_ROOT = ["/news/pts", "/news/hakka", "/news/titv"]

def retrieve_by_file_path(file_path):
    print("DO retrieve_by_file_path %s" %file_path)

def retrieve_by_file_no(file_no, hsm='HSM3', target_dir=None):
    print("DO retrieve_by_file_no %s" %file_no)
    reExt = re.compile(".*\.mxf$", re.IGNORECASE)
    HSM = config.HSM_CONFIG.HSM[hsm]
    HSM4 = config.HSM_CONFIG.HSM['HSM4']
    HSM5 = config.HSM_CONFIG.HSM['HSM5']
    login = config.HSM_CONFIG.login
    passwd = config.HSM_CONFIG.password

    year = file_no[0:4]
    mon = file_no[4:6]
    day = file_no[6:8]

    vol_status = ''
    in_gpfs = False
    found = False
    for r in TARGET_ROOT:
        try:
            gpfs_dir = r + "/" + year + "/" + mon + "/" + day + "/" + file_no + "/"
            print("check: %s" %gpfs_dir)
            plink_args = ['plink',  '-ssh', HSM, '-l', login, '-pw', passwd] 
            ls_args = ['ls', gpfs_dir]
            plink_args.extend(ls_args)
            proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
            rc = proc.returncode
            exit_code = proc.wait()
            lines = proc.stdout.readlines()
            for l in lines:
                l = l.decode('utf-8')
                l = l.rstrip()
                news_path = gpfs_dir + "/" + l
                print("Found %s" %news_path)
                file_info_dict = dsmls(news_path) #check if vol available
                for f in file_info_dict.keys():
                    file_info_list = file_info_dict[f]
                    file_status = file_info_list[1]
                    vol_status = file_info_list[2]
                    if (file_status in ('p', 'r')):
                        ptsutils3.cp(news_path, target_dir, target_dir)
                    if (file_status == 'm'):
                        if (vol_status == 'o-a'):
                            dsmrecall('HSM3', news_path) #do recall file
                            if (target_dir != None):
                                ptsutils3.cp(news_path, target_dir, target_dir)

                found = True
                break

            #dsmrecall_args = ['dsmrecall', news_path]

        except Exception as e:
            template = "Exception {0} occur : {1!r}"
            message = template.format(type(e).__name__, e.args)
            print(message)

        if (found):
            break

def retrieve_by_file_list(in_file, hsm = 'HSM5', target_dir = None):
    print("Do retrieve_by_file_list")
    fh = open(in_file, "r")
    lines = fh.readlines()
    for l in lines:
        file_no = l.rstrip()
        retrieve_by_file_no(file_no, target_dir = target_dir)



def usage():
    usage_str = """
NAME
    news_retrieve - Retrieve news file from GPFS, ISILON etc

SYNOPSIS
    news_retrieve [OPTION]

DESCRIPTION

    --file_no=[news file number]
        ex : --file_no=201512150010065

    --dsmrecall=[news file number]
    
    -h : help

    --to=[target dir]
        copy recalled file to target dir
    """


def main(argv):
    DO_RETRIEVE_BY_FILE_NO = False
    DO_RETRIEVE_BY_FILE_PATH = False
    DO_DSMRECALL_TO = False
    DO_RETRIEVE_BY_FILE_LIST = False

    try:
        opts, args = getopt.getopt(argv, "h", ["file=", "hsm=", "file_no=", "fpath=", "to=", "dsmrecall="])

    except Exception as e:
        template = "Exception {0} occur : {1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)

        sys.exit(2)
    
    target_dir = None
    for opt, arg in opts:
        if (opt == '-h'):
            usage()
            sys.exit()
        elif (opt == "--fpath"):
            file_path = arg
        elif (opt == "--file_no"):
            file_no = arg
            DO_RETRIEVE_BY_FILE_NO = True
        elif (opt == "--dsmrecall"):
            file_no = arg
            DO_RETRIEVE_BY_FILE_NO = True
        elif (opt == "--hsm"):
            hsm = arg
        elif (opt == "--file"):
            in_file = arg
            DO_RETRIEVE_BY_FILE_LIST = True
        elif (opt == "--to"):
            DO_DSMRECALL_TO = True
            target_dir = arg

        
    if (DO_RETRIEVE_BY_FILE_PATH):
        retrieve_by_file_path(file_path)
    elif (DO_RETRIEVE_BY_FILE_NO):
        retrieve_by_file_no(file_no, target_dir=target_dir)
    elif (DO_RETRIEVE_BY_FILE_LIST):
        retrieve_by_file_list(in_file, target_dir=target_dir)


if (__name__ == "__main__"):
    main(sys.argv[1:])