#_*_ Encoding:UTF-8 _*_ 
import os
import sys
import pyodbc
from datetime import datetime, timedelta, date
import csv
import subprocess
import config
from shutil import copyfile

TARGET_DIR = "\\\\10.13.200.15\\ShareFolder\\Borrow\\"

server = config.NEWNEWS_CONFIG.db_server
database = config.NEWNEWS_CONFIG.database
username = config.NEWNEWS_CONFIG.db_user[0]
password = config.NEWNEWS_CONFIG.db_user[1]
conn_str = 'DRIVER={{ODBC Driver 13 for SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)

def copy_archived_file_by_db():
    cnxn = pyodbc.connect(conn_str)
    cursor = cnxn.cursor()
    #tsql = ("select VideoID, ArchivePath, ArchiveName, MP4Path, MP4Name from VideoMeta;")
    tsql = ("select top 2 VideoId, UserID ,ArchivePath, ArchiveNAme, Slogan from vwBorrowList where BorrowStatus=3 order by BorrowID desc;")

    #query all data save in archive_info, key = ArchiveName
    count = 0
    copy_list = []
    with cursor.execute(tsql):
        row = cursor.fetchone()
        while row:
            video_id = str(row[0])
            user_id = str(row[1])
            archive_path = str(row[2])
            archive_name = str(row[3])
            slogan= str(row[4])

            src_file = os.path.join(archive_path, archive_name)
            target_file = TARGET_DIR + "51204\\" + slogan + '.mxf'
            copy_pair = [src_file, target_file]
            copy_list.append(copy_pair)
            
            row = cursor.fetchone()
    cnxn.close() #end of query_video_meta()

    return copy_list



def copy_archived_by_list():

    inputf = sys.argv[1] # file list
    with open(inputf, 'r', encoding='utf-8', newline='') as csvfile:
        csvreader = csv.reader(csvfile, delimiter='|')
        for row in csvreader:
            archive_dir = row[0].strip()
            archive_name = row[1].strip()
            slogan = row[2].strip()
            user_id = '51204'
            #print('| '. join(row))
            archive_path = archive_dir + '\\' + archive_name
            target = TARGET_DIR + user_id + '\\' + slogan + '.mxf'
            print("copy " + archive_path + " to " + target)
            proc = subprocess.Popen(['powershell.exe', 'new-item', '-path', target, '-itemtype file'], stdout=subprocess.PIPE, shell=True)
            res = proc.stdout.readlines()
            print(res)

copy_list = copy_archived_file_by_db()
for cp in copy_list:
    src = cp[0]
    target = cp[1]
    print("copy " + cp[0] + " to " + cp[1])
    copyfile(src, target)