#!/usr/bin/python

import os.path
import os
import sys
import re
import ptsutils
#from enum import Enum

class Status:
    RECALL_OK = 0
    RECALL_FAIL = 1

inputF = sys.argv[1]
f = open(inputF, "r")
out = f.readlines()
targetDir = '/mnt/mamnas1/Approved'
status = "RECALL_FAIL"
item = []
count = 1
copied = 1
num = len(out)
print(str(num) + ' files to be processed')
for line in out:
    line = line.rstrip()
    #arr = line.split('\\t')
    arr = re.split('\s+', line)
    videoID = arr[0]
    sambaPath = arr[1]
    if (len(videoID) == 8):
       videoID = videoID[2:]

    item = sambaPath.split('\\')
    #print item
    tsmPath = ''
    for i in range(4, len(item), 1):
        #print item[i]
        tsmPath += '/' + item[i]

    #subprocess.call(["demrecall", "tsmPath"])
    cmd = 'dsmrecall ' + tsmPath
    #print cmd + ' ' + videoID
    targetObj = targetDir + '/' + videoID + '.mxf'
    if (not os.path.isfile(targetObj)):
        # recall gpfs file
        print("recall " + tsmPath)
        os.system(cmd)
        #check if the file in GPFS is recalled successful
        size_arr = ptsutils.get_gpfs_size(tsmPath)
        if (size_arr):
            actuall_size = size_arr[0]  #the actuall file size of file on Tape
            resident_size = size_arr[1] #the file size that recalled to online storage
            if (actuall_size == resident_size):
                status = Status.RECALL_OK
            else:
                status = Status.RECALL_FAIL

        if (not os.path.isfile(targetObj)):
            if (status == Status.RECALL_OK):
                SEQ = '(' + str(count) + ',' + str(copied) + ',' + str(num) + ') :'
                print(SEQ + 'copy ' + tsmPath + ' to ' + targetObj)
                os.system('cp ' +  tsmPath + ' ' +  targetObj + ' -v')
                copied += 1
            else:
                SEQ = '(' + str(count) + ',' + str(num) + ') :'
                print(SEQ + 'Recall fail ' + targetObj)

        else:
            SEQ = '(' + str(count) + ',' + str(num) + ') :'
            print(SEQ + 'Found ' + targetObj)
    else:
        SEQ = '(' + str(count) + ',' + str(num) + ') :'
        print(SEQ + 'Found ' + targetObj)

    count += 1
    print("\n\n")
