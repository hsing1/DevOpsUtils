# -*- coding: UTF-8 -*-

import os
import sys
import pyodbc
import config
import shutil
import getopt
import time
import re
from datetime import datetime
from check_file_status_win import check_by_file_no, check_by_file_list
import subprocess
import ptsutils3


server = config.NEWNEWS_CONFIG.db_server
database = config.NEWNEWS_CONFIG.database 
username = config.NEWNEWS_CONFIG.db_user[0]
password = config.NEWNEWS_CONFIG.db_user[1]
conn_str = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)


#input arguments
#video_id : 004XFG
#file_no : G000000100010025 | P000000100010025

#output:
#\\10.13.200.30\gpfs\ptstv, \\10.13.200.30\gpfs\hakkatv. \\10.13.200.30\gpfs\titv
#\\10.13.200.30\gpfs\promo


def get_prog_archive_info():
    TARGET_DIR = '\\\\isilon.pts.org.tw\MAM_ARCHIVE'
    server = config.MAM_CONFIG.db_server
    database = config.MAM_CONFIG.database
    username = config.MAM_CONFIG.db_user1[0]
    password = config.MAM_CONFIG.db_user1[1]
    connStr = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
    tsql = ("select PROG_ID, FSVIDEO_PROG, FSFILE_NO, FCFILE_STATUS, NAME, FSFILE_PATH_H from VW_TBLOG_VIDEO_PROG_ALL order by PROG_ID, FSFILE_NO")
    cnxn = pyodbc.connect(connStr)
    cursor = cnxn.cursor()

    print("Get program information ...")
    archive_info = {}
    with cursor.execute(tsql):
        row = cursor.fetchone()
        while row:
            record = ""
            prog_id = str(row[0].rstrip())
            video_id = str(row[1].rstrip())
            file_no = str(row[2].rstrip())
            file_status = str(row[3].rstrip())
            name = str(row[4].rstrip())
            #replace special character with '-'
            name = re.sub('[\\/:\*\?"\<\>\|\t]', '-', name)
            file_path = str(row[5].rstrip())
            str_arr = file_path.split('\\')
            prog_dir = prog_id + "-" + name
            file_name = file_no + "_" + video_id
            gpfs_path_list_l = [] # linux format
            gpfs_path_list_w = [] # windows format
            for item in str_arr[4:]:
                gpfs_path_list_l.append('/')
                gpfs_path_list_l.append(item)
                gpfs_path_list_w.append('\\')
                gpfs_path_list_w.append(item)

            gpfs_path_l = ''.join(gpfs_path_list_l)
            gpfs_path_w = ''.join(gpfs_path_list_w)
            channel = str_arr[4]
            year = str_arr[6]
            new_gpfs_path_l = "/{c}/HVideo/{y}/{p}/{f}.mxf".format(c=channel, y=year, p=prog_dir, f=file_name)
            new_gpfs_path_w = "\\{c}\\HVideo\\{y}\\{p}\\{f}.mxf".format(c=channel, y=year, p=prog_dir, f=file_name)
            out = prog_dir + ":" + file_name + ":" + file_status + ":" + file_path + ":" + gpfs_path_l + ":" + new_gpfs_path_l + ":" + new_gpfs_path_w + "\n" 
            #print("TARGET_DIR = " + TARGET_DIR)
            #print(new_gpfs_path_w)
            #gpfs_path_w = os.path.join(TARGET_DIR, new_gpfs_path_w)
            #gpfs_path_l = os.path.join(TARGET_DIR, new_gpfs_path_l)
            gpfs_path_w = TARGET_DIR + new_gpfs_path_w
            gpfs_path_l = TARGET_DIR + "/" + new_gpfs_path_l
            path_list = [gpfs_path_w, gpfs_path_l]
            archive_info[file_name]  = path_list
            #print (file_name + ":" + gpfs_path_w + ":" + gpfs_path_l)
            row = cursor.fetchone()
    cnxn.close()

    return archive_info




#Todo: modify, dump all info to arrar
#To get programe and video_id and file_no information
def get_prog_file_info_org(type, build_file_struct):
    TARGET_DIR = '\\\\10.13.200.30\\GPFS\\MAM_ARCHIVE'
    server = config.MAM_CONFIG.db_server
    database = config.MAM_CONFIG.database
    username = config.MAM_CONFIG.db_user1[0]
    password = config.MAM_CONFIG.db_user1[1]
    connStr = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)

    date_time = time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime())
    current_date = time.strftime('%Y-%m-%d', time.localtime())
    cnxn = pyodbc.connect(connStr)
    cursor = cnxn.cursor()

    if (type == '0'): #dump all
        print("dump all video info")
        tsql = ("select PROG_ID, FSVIDEO_PROG, FSFILE_NO, FCFILE_STATUS, NAME, FSFILE_PATH_H from VW_TBLOG_VIDEO_PROG_ALL order by PROG_ID, FSFILE_NO")
        f_output = TARGET_DIR + "tblog_video_all_" + date_time + ".txt"
    elif (type == '1'): #dump new updated today
        print("dump new updated video info")
        tsql = ("select PROG_ID, FSVIDEO_PROG, FSFILE_NO, FCFILE_STATUS, NAME, FSFILE_PATH_H from VW_TBLOG_VIDEO_PROG_ALL where FDUPDATED_DATE >= '" + current_date +  "' order by PROG_ID, FSFILE_NO")
        f_output = TARGET_DIR + "tblog_video_updated_" + date_time + ".txt"
    elif (type == '2'): # new created today
        print("dump new created video info")
        tsql = ("select PROG_ID, FSVIDEO_PROG, FSFILE_NO, FCFILE_STATUS, NAME, FSFILE_PATH_H from VW_TBLOG_VIDEO_PROG_ALL where FDCREATED_DATE >= '" + current_date +  "' order by PROG_ID, FSFILE_NO")
        f_output = TARGET_DIR + "tblog_video_created_" + date_time + ".txt"
    else:
        print("Wrong type")
        return 1

    fh = open(f_output, "w", encoding='utf-8')
    count = 1
    with cursor.execute(tsql):
        row = cursor.fetchone()
        while row:
            record = ""
            prog_id = str(row[0].rstrip())
            video_id = str(row[1].rstrip())
            file_no = str(row[2].rstrip())
            file_status = str(row[3].rstrip())
            name = str(row[4].rstrip())
            #replace special character with '-'
            name = re.sub('[\\/:\*\?"\<\>\|\t]', '-', name)
            file_path = str(row[5].rstrip())
            str_arr = file_path.split('\\')
            prog_dir = prog_id + "-" + name
            file_name = file_no + "_" + video_id
            gpfs_path_list_l = [] # linux format
            gpfs_path_list_w = [] # windows format
            for item in str_arr[4:]:
                gpfs_path_list_l.append('/')
                gpfs_path_list_l.append(item)
                gpfs_path_list_w.append('\\')
                gpfs_path_list_w.append(item)

            gpfs_path_l = ''.join(gpfs_path_list_l)
            gpfs_path_w = ''.join(gpfs_path_list_w)
            channel = str_arr[4]
            year = str_arr[6]
            new_gpfs_path_l = "/{c}/HVideo/{y}/{p}/{f}.mxf".format(c=channel, y=year, p=prog_dir, f=file_name)
            new_gpfs_path_w = "\\{c}\\HVideo\\{y}\\{p}\\{f}.mxf".format(c=channel, y=year, p=prog_dir, f=file_name)
            out = prog_dir + ":" + file_name + ":" + file_status + ":" + file_path + ":" + gpfs_path_l + ":" + new_gpfs_path_l + ":" + new_gpfs_path_w + "\n" 
            #print(str(count) + ":" + out)
            fh.writelines(out)
            #if (os.path.isdirs())

            if (build_file_struct == 1): #create file
                gpfs_path = TARGET_DIR + new_gpfs_path_w
                res = create_gpfs_file(gpfs_path)

            row = cursor.fetchone()


def create_gpfs_file(gpfs_path):
    folder = os.path.dirname(gpfs_path)
    if (not os.path.exists(folder)):
        os.makedirs(folder)

    stud_path = gpfs_path + ".stud"
    if (not os.path.exists(gpfs_path) and not os.path.exists(stud_path)):
        print("create " + stud_path)
        try:
            fh = open(stud_path, "w")
            return True
        except Exception as e:
            print(str(e)) 
            return False

#purge file FCFILE_STATUS != T
def purge_archive_file():
    print("Todo")

#migrate G000000100010008_0000001Q.mxf to G000000100010008_0000001Q.mxf.stud
def migrate_archive_file():
    print("Todo")

#get archive file and copy to //isilon.pts.org.tw/MAM_ARCHIVE
def archive_file(archive_info, file_path):
    base_name = os.path.basename(file_path)
    re_obj = re.compile(r'([G|P]\d{15})_([\d|\w]{8})')
    archived_status = []
    
    res = os.path.splitext(base_name)
    fname = res[0]
    # videoID
    if (len(fname) == 6):
        fname = "00" + fname

    fsfile_no = ''
    video_id = ''
    FOUND = False
    #base_name could be FSFILE_NO, or VIDEO_ID
    for f in archive_info:
        # get file_no and video_id
        res = re_obj.match(f)
        if (res):
            fsfile_no = res.group(1)
            video_id = res.group(2)
        else:
            print("Warning : cannot match " + f)

        #print(fname + "_" + video_id + "_" + fsfile_no)
        if ((fname == video_id) or (fname == fsfile_no)):
            path_list = archive_info[f]
            archive_path = path_list[0]
            create_gpfs_file(archive_path)
            stud_file = archive_path +  '.stud'
            if (not os.path.isfile(archive_path)):
                print("Archive:" + file_path + " to " + archive_path)
                cmd = "copy \"{s}\" \"{t}\"".format(s=file_path, t=archive_path)
                print(cmd)
                os.system(cmd)

                if (os.path.isfile(stud_file)):
                    os.remove(stud_file)

                archived_status = [file_path, archive_path]
            FOUND = True
            break
    
    return archived_status

# scan watch folder
def watch_folder(archive_info, watch_dir):
    log = "mam_archive.log"
    fh = open(log, "a", encoding='utf-8')
    archive_status = []
    template = "[{0}] {1} : copy {2} to {3}\n"
    msg = ''

    count = 1
    for r, ds, fs in os.walk(watch_dir):
        for f in fs:
            res = os.path.splitext(f)
            base_name = res[0]
            ext = res[1].lower()

            if (ext != '.mxf'):
                continue

            fpath = os.path.join(r, f)
            #print("%i : " %count )
            archive_status = archive_file(archive_info, fpath)
            if (len(archive_status) == 2):
                timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S") 
                msg = template.format(count, timestamp, archive_status[0], archive_status[1])
                print(msg)
                fh.writelines(msg)
                count += 1

#retrieve file from given file list
def retrieve_by_file_list(archive_info, list_file, target_dir):
    flist = check_by_file_list(list_file)
    for fno in flist:
        retrieve(archive_info, fno, target_dir)


def retrieve_by_program_id(archive_info, prog_id, episode):
    print("Retrieve by program id")

    
# file_no = P201811006930002 or G200721705720002 or 00ABCD or 0000ABCD
def retrieve(archive_info, file_no, target_dir):
    print("Retrieve file")
    re_videoid = re.compile(r'^[a-zA-Z0-9]{6,8}$')
    re_file_no = re.compile(r'^[G|P]\d{15}$')
    re_archive = re.compile(r'([G|P]\d{15})_([a-zA-Z0-9]{8})')
    video_id = ''
    fsfile_no = ''
    target_id = ''
    retrieve_status = []
    #download_dir = '\\\\10.13.200.25\\MAMDownload\\hsing1'
    
    res1 = re_videoid.match(file_no)
    res2 = re_file_no.match(file_no)
    if (res1 is not None):
        video_id = res1[0].zfill(8)
        target_id = video_id
    elif (res2 is not None):
        fsfile_no = res2[0]
        target_id = fsfile_no
    else:
        print("Error : Cannot identify file id! %s" %file_no)
    
    print("Retrieve file %s" %target_id)
    for f in archive_info:
        # get file_no and video_id
        if (target_id in f):
            path_list = archive_info[f]
            archive_path = path_list[0]
            stud_file = archive_path +  '.stud'
            if (os.path.isfile(archive_path)):
                cmd = "copy \"{s}\" \"{t}\"".format(s=archive_path, t=target_dir)
                os.system(cmd)
                FOUND = True
                break
            else:
                print("%s not found!" %archive_path)

def dsmls_list(hsm, flist):
    fh = open(flist, "r")
    lines = fh.readlines()
    for l in lines:
        fpath = l.rstrip()
        dsmls(hsm, fpath)

def dsmls(hsm, fpath):
    HSM = config.HSM_CONFIG.HSM[hsm]
    print(HSM)
    login = config.HSM_CONFIG.login
    passwd = config.HSM_CONFIG.password
    tsm_login = config.TSM_CONFIG.tsm_user1[0]
    tsm_passwd = config.TSM_CONFIG.tsm_user1[1]
    tsm_path = ptsutils3.mampath2tsmpath(fpath)

    print("dsmls %s" %tsm_path)

    plink_args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd] 
    dsmls_args = ['dsmls', tsm_path, '| sed 1,8d']
    plink_args.extend(dsmls_args)
    proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
    lines = proc.stdout.readlines()
    rc = proc.returncode
    exit_code = proc.wait()
    print(exit_code)
    #print("Return code %i" %rc)
    for  l in lines:
        #print(l) => b'  Client Version 6, Release 3, Level 0.0  \n'
        l = l.decode('utf-8')
        print(l)
        res = l.split()
        #print(res)
        status = res[3]
        file_res = os.path.splitext(res[4])
        file_no = file_res[0]
        print(file_no)
        if (status == 'p'):
            print("File %s is online (migrated)" %file_no)
        elif (status == 'r'):
            print("File %s is online (not migrated)" %file_no)
        elif (status == 'm'):
            print("File %s is migrated" %file_no)
            status_info = check_by_file_no(file_no)
            status_dict = status_info[1]
            for v in status_dict.keys():
                vol_status = status_dict[v]
                if (vol_status == 'off-a'):
                    print("%s is off-site, available" %v)
                elif (vol_status == 'off-u'):
                    print("%s is off-site, unavailable" %v)
                elif (vol_status == 'o-a'):
                    print("%s is on-site, available" %v)
                    print("Do dsmrecall")
                    #dsmrecall(tsm_path)
                elif (vol_status == 'o-u'):
                    print("%s is on-site, unavailable" %v)

# recall by file list, and copy to target dir
def dsmrecall_list_to(hsm, flist, target_dir):
    fh = open(flist, "r")
    lines = fh.readlines()
    for l in lines:
        fpath = l.rstrip()
        dsmrecall_to(hsm, fpath, target_dir)


#recall and copy to target folder 
def dsmrecall_to(hsm, fpath, target_dir):
    HSM = config.HSM_CONFIG.HSM[hsm]
    login = config.HSM_CONFIG.login
    passwd = config.HSM_CONFIG.password
    tsm_login = config.TSM_CONFIG.tsm_user1[0]
    tsm_passwd = config.TSM_CONFIG.tsm_user1[1]
    tsm_path = ptsutils3.mampath2tsmpath(fpath)

    print("recall %s on %s, copy to %s" %(tsm_path, hsm, target_dir))

    plink_args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd] 
    dsmls_args = ['dsmrecall', tsm_path, ';']
    copy_args = ['cp', tsm_path,  target_dir, '-v']
    plink_args.extend(dsmls_args)
    plink_args.extend(copy_args)
    proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
    lines = proc.stdout.readlines()
    for l in lines:
        print(l.decode('utf-8').rstrip())

def dsmrecall_list(hsm, flist):
    fh = open(flist, "r")
    lines = fh.readlines()
    for l in lines:
        fpath = l.rstrip()
        dsmrecall(hsm, fpath)


def dsmrecall(hsm, fpath):
    HSM = config.HSM_CONFIG.HSM[hsm]
    login = config.HSM_CONFIG.login
    passwd = config.HSM_CONFIG.password
    tsm_login = config.TSM_CONFIG.tsm_user1[0]
    tsm_passwd = config.TSM_CONFIG.tsm_user1[1]
    tsm_path = ptsutils3.mampath2tsmpath(fpath)

    print("recall %s on %s" %(tsm_path, hsm))

    plink_args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd] 
    dsmls_args = ['dsmrecall', tsm_path]
    plink_args.extend(dsmls_args)
    proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
    lines = proc.stdout.readlines()
    for l in lines:
        print(l.decode('utf-8').rstrip())

def check_hdulload_status():
    HDUPLOAD_PATH = config.MAM_STORAGE.HDUPLOAD[0]
    #re_videoid = re.compile(r'[\d|\w]{6, 8}')
    #archive_info = get_prog_archive_info()
    re_videoid = re.compile(r'.*([a-zA-Z0-9]{6,8}$)')

    for r, ds, fs in os.walk(HDUPLOAD_PATH):
        for f in fs:
            (f_name, ext) = os.path.splitext(f)
            res = re_videoid.match(f_name)
            if (res):
                video_id = res.group(1)
                archive_status = get_file_status('00' + video_id)
                if (archive_status != 'T'):
                    msg = "{}, {}".format(video_id, archive_status)
                    print(msg)

def get_file_status(video_id):
    server = config.MAM_CONFIG.db_server
    database = config.MAM_CONFIG.database
    username = config.MAM_CONFIG.db_user1[0]
    password = config.MAM_CONFIG.db_user1[1]
    connStr = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
    tsql = ("select FCFILE_STATUS from TBLOG_VIDEO where FSVIDEO_PROG = ?")
    cnxn = pyodbc.connect(connStr)
    cursor = cnxn.cursor()
    file_status = ''

    print("Get program information ...")
    archive_info = {}
    with cursor.execute(tsql, video_id):
        row = cursor.fetchone()
        while row:
            file_status = str(row[0].rstrip())
            row = cursor.fetchone()
    
    cnxn.close()
        
    return file_status

def get_tsmjob_status(tsm_id):
    server = config.MAM_CONFIG.db_server
    database = config.MAM_CONFIG.database
    username = config.MAM_CONFIG.db_user1[0]
    password = config.MAM_CONFIG.db_user1[1]
    connStr = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
    tsql = ("select FNSTATUS from TBTSM_JOB where FNTSMJOB_ID = ?")
    cnxn = pyodbc.connect(connStr)
    cursor = cnxn.cursor()
    tsm_status = ''

    with cursor.execute(tsql, tsm_id):
        row = cursor.fetchone()
        while row:
            tsm_status = row[0]
            row = cursor.fetchone()
    
    cnxn.close()
        
    return tsm_status


def usage():
    usage_str = """
Archive file
    -f, --file : specify file to be archived
    -w, --watch : specify watch folder to be archived
    -d : specify folder to be archived
    -r, --retrieve : retrieve by file number
    --rfile=retrive_file : retrieve by file list
    --dsmls=TSM_PATH : check TSM file status
    --dsmrecall=file_path : recall file
    --hsm=HSM# : specify HSM server (default HSM4)
    """

def main(argv):
    WATCH = False
    CONTINUE_WATCH = False
    RETRIEVE_MODE = False
    DO_RETRIEVE_LIST_MODE = False
    DO_ARCHIVE_FILE = False
    DO_ARCHIVE_DIR = False
    DO_DSMLS = False
    DO_DSMLS_LIST = False
    DO_DSMRECALL = False
    DO_DSMRECALL_TO = False
    DO_DSMRECALL_LIST = False
    DO_CHECK_HDUPLOAD = False
    file_no = ''
    watch_dir = ''
    target_dir = ''
    hsm = 'HSM4' #default

    try:
        opts, args = getopt.getopt(argv, "d:f:r:w:t:", 
        [
        "fdsmls=", 
        "dsmls=", 
        "dir=", 
        "target_dir=", 
        "file=", 
        "watch=", 
        "retrieve=", 
        "rfile=", 
        "tcase=", 
        "dsmrecall=", 
        "hsm=", 
        "recallto=", 
        "reset_tsmjob=" #reset tsmjob status by tsm_id
        "fdsmrecall=", 
        "check_hdupload",
        "tsm_id=" # get tsm job status, 0, 1, 2, 3 (success), 5 (fail)
        ])

    except Exception as e:
        template = "Exception {0} occur : {1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)

        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-f", "--file"): # copy file to archive folder
            DO_ARCHIVE_FILE = True
            file_path = arg
        elif opt in ("-d", "--dir"):
            DO_ARCHIVE_DIR = True
        elif opt in ("-w", "--watch"): # specify watch folder
            watch_dir = arg
            WATCH = True
        elif opt in ("-r", "--retrieve"):
            file_no = arg
            RETRIEVE_MODE = True
        elif opt in ("-t", "--target_dir"):
            target_dir = arg
        elif opt in ("--rfile"):
            retrieve_file = arg
            DO_RETRIEVE_LIST_MODE = True
        elif opt in ("--tcase"):
            test_case(int(arg))
            sys.exit(2)
        elif opt in ("--fdsmls"):
            DO_DSMLS_LIST = True
            flist = arg
        elif opt in ("--dsmls"):
            DO_DSMLS = True
            tsm_path = arg
        elif opt in ("--dsmrecall"):
            DO_DSMRECALL = True
            tsm_path = arg
        elif opt in ("--fdsmrecall"):
            DO_DSMRECALL_LIST = True
            flist = arg
        elif opt in ("--hsm"):
            hsm = arg
        elif opt in ("--recallto"):
            DO_DSMRECALL_TO = True
            target_dir = arg
        elif opt in ("--check_hdupload"): #check HDUpload archive status
            DO_CHECK_HDUPLOAD = True
        elif opt in ("--tsm_id"): # get tsm job status
            DO_CHECK_TSMJOB_BY_TSMID = True
            tsm_id = arg
    
    if (WATCH):
        while (1):
            archive_info = get_prog_archive_info()
            watch_folder(archive_info, watch_dir)
            print("Sleeping ...")
            time.sleep(3000)

    elif (DO_ARCHIVE_FILE):
        archive_info = get_prog_archive_info()
        archive_file(archive_info, file_path)

    elif (DO_ARCHIVE_DIR):
        archive_info = get_prog_archive_info()
        watch_folder(archive_info, watch_dir)

    elif (RETRIEVE_MODE):
        archive_info = get_prog_archive_info()
        retrieve(archive_info, file_no, target_dir)

    elif (DO_RETRIEVE_LIST_MODE):
        archive_info = get_prog_archive_info()
        retrieve_by_file_list(archive_info, retrieve_file, target_dir)
    
    elif (DO_DSMLS):
        dsmls(hsm, tsm_path)
    
    elif (DO_DSMLS_LIST):
        dsmls_list(hsm, flist)
    
    elif (DO_DSMRECALL and not DO_DSMRECALL_TO):
        dsmrecall(hsm, tsm_path)
    
    elif (DO_DSMRECALL and DO_DSMRECALL_TO):
        dsmrecall_to(hsm, tsm_path, target_dir)
    
    elif (DO_DSMRECALL_LIST and DO_DSMRECALL_TO):
        dsmrecall_list_to(hsm, flist, target_dir)

    elif (DO_DSMRECALL_LIST and not DO_DSMRECALL_TO): #--fdsmrecall= -recallto=
        dsmrecall_list(hsm, flist)

    elif (DO_CHECK_HDUPLOAD):
        check_hdulload_status()

    elif (DO_CHECK_TSMJOB_BY_TSMID):
        tsm_status = get_tsmjob_status(tsm_id)
        print("%s : %i" %(tsm_id, tsm_status))

def test_case(case_num):
    archive_info = {}
    TARGET_DIR = '\\\\isilon.pts.org.tw\MAM_ARCHIVE'
    if (case_num == 1):
        archive_info = get_prog_archive_info()
        prog_id = '0000060'
        episode = 1531
        #archive_info = get_prog_archive_info()
        file_no = prog_id + str(episode).zfill(4)
        print(file_no)
        for f in archive_info.keys():
            if (file_no in f):
                print(f)
                print(archive_info[f])
                #print(v)
        ## end case 1
    if (case_num == 2):
        HSM = config.HSM_CONFIG.HSM['HSM4']
        login = config.HSM_CONFIG.login
        passwd = config.HSM_CONFIG.password
        tsm_login = config.TSM_CONFIG.tsm_user1[0]
        tsm_passwd = config.TSM_CONFIG.tsm_user1[1]

        plink_args = ['plink', '-ssh', HSM, '-l', login, '-pw', passwd, 'cd ~; ls -l; touch aaa'] 
        print(plink_args)
        proc = subprocess.Popen(plink_args, stdout=subprocess.PIPE, shell=True)
        #cmd = 'plink -ssh 10.13.210.15 -l root -pw TSMP@ssw0rdTSM  cd /mnt/MAMDownload; ls -l' 
        #proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        lines = proc.stdout.readlines()
        for l in lines:
            print(l)

    if (case_num == 3):
        prog_id = '0000060'
        episode = 1
        server = config.MAM_CONFIG.db_server
        database = config.MAM_CONFIG.database
        username = config.MAM_CONFIG.db_user1[0]
        password = config.MAM_CONFIG.db_user1[1]
        connStr = 'DRIVER={{SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
        tsql = ("select PROG_ID, FNEPISODE, FSVIDEO_PROG, FSFILE_NO, FCFILE_STATUS, NAME, FSFILE_PATH_H from VW_TBLOG_VIDEO_PROG_ALL order by PROG_ID, FSFILE_NO")
        cnxn = pyodbc.connect(connStr)
        cursor = cnxn.cursor()

        print("Get program information ...")
        archive_info = {}
        with cursor.execute(tsql):
            row = cursor.fetchone()
            while row:
                record = ""
                prog_id = str(row[0].rstrip())
                episode = row[1]
                video_id = str(row[2].rstrip())
                file_no = str(row[3].rstrip())
                file_status = str(row[4].rstrip())
                name = str(row[5].rstrip())
                #replace special character with '-'
                name = re.sub('[\\/:\*\?"\<\>\|\t]', '-', name)
                file_path = str(row[6].rstrip())
                str_arr = file_path.split('\\')
                prog_dir = prog_id + "-" + name
                file_name = file_no + "_" + video_id
                gpfs_path_list_l = [] # linux format
                gpfs_path_list_w = [] # windows format
                for item in str_arr[4:]:
                    gpfs_path_list_l.append('/')
                    gpfs_path_list_l.append(item)
                    gpfs_path_list_w.append('\\')
                    gpfs_path_list_w.append(item)

                gpfs_path_l = ''.join(gpfs_path_list_l)
                gpfs_path_w = ''.join(gpfs_path_list_w)
                channel = str_arr[4]
                year = str_arr[6]
                new_gpfs_path_l = "/{c}/HVideo/{y}/{p}/{f}.mxf".format(c=channel, y=year, p=prog_dir, f=file_name)
                new_gpfs_path_w = "\\{c}\\HVideo\\{y}\\{p}\\{f}.mxf".format(c=channel, y=year, p=prog_dir, f=file_name)
                out = prog_dir + ":" + file_name + ":" + file_status + ":" + file_path + ":" + gpfs_path_l + ":" + new_gpfs_path_l + ":" + new_gpfs_path_w + "\n" 
                #print("TARGET_DIR = " + TARGET_DIR)
                #print(new_gpfs_path_w)
                #gpfs_path_w = os.path.join(TARGET_DIR, new_gpfs_path_w)
                #gpfs_path_l = os.path.join(TARGET_DIR, new_gpfs_path_l)
                gpfs_path_w = TARGET_DIR + new_gpfs_path_w
                gpfs_path_l = TARGET_DIR + "/" + new_gpfs_path_l
                path_list = [gpfs_path_w, gpfs_path_l]
                archive_info[file_name]  = path_list
                #print (file_name + ":" + gpfs_path_w + ":" + gpfs_path_l)
                row = cursor.fetchone()
        cnxn.close()

        file_no = prog_id + str(episode)
        for f in archive_info.keys():
            if (file_no in f):
                print(f)
                print(archive_info[f])
                #print(v)
        ## end case 2

if (__name__ == "__main__"):
    main(sys.argv[1:])



#get archive file and copy to \\10.13.200.15\gpfs
def archive_file_org(file_path):
    TARGET_DIR = "\\\\10.13.200.30\\GPFS\\MAM_ARCHIVE"
    base_name = os.path.basename(file_path)
    re_obj = re.compile(r'([G|P]\d{15})_([\d|\w]{8})\.mxf')
    re_video = re.compile(r'[\d|\w]{6, 8}')
    
    res = os.path.splitext(base_name)
    fname = res[0]
    if (len(fname) == 6):
        fname = "00" + fname

    fsfile_no = ''
    video_id = ''
    FOUND = False
    #base_name could be FSFILE_NO, or VIDEO_ID
    for r, ds, fs in os.walk(TARGET_DIR):
        for f in fs:
            res = re_obj.match(f)
            if (res):
                fsfile_no = res.group(1)
                video_id = res.group(2)
            else:
                print("Warning : cannot match " + os.path.join(r, f))

            #print(fname + "_" + video_id + "_" + fsfile_no)
            if ((fname == video_id) or (fname == fsfile_no)):
                full_path = os.path.join(r, f)
                res = os.path.splitext(full_path)
                new_path = res[0]
                if (not os.path.isfile(new_path)):
                    cmd = "copy {s} {t}".format(s=file_path, t=new_path)
                    print(cmd)
                    os.system(cmd)
                    os.remove(full_path)
                FOUND = True
                break
        
        if (FOUND):
            break