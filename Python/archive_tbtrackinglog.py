#_*_ Encoding:UTF-8 _*_ 
import os
import sys
import pyodbc
from datetime import datetime, timedelta, date
import csv

server = '10.13.210.33'
database = 'MAM'
username = 'mam_admin'
password = 'ptsP@ssw0rd'
conn_str = 'DRIVER={{ODBC Driver 13 for SQL Server}};SERVER={s};PORT=1443;DATABASE={d};UID={u};PWD={p}'.format(s = server, d = database, u = username, p = password)
cnxn = pyodbc.connect(conn_str)
cursor = cnxn.cursor()
#TARGET_DIR = '\\\\10.13.200.98\\mamspace\\LOG\\'
TARGET_DIR = '\\\\10.13.200.97\\mam_data\\MAMBAKUP\\TBTRACKINGLOG\\'


def extract_log(begin_day_obj, end_day_obj):
    begin_day = begin_day_obj.strftime("%Y-%m-%d")
    end_day = end_day_obj.strftime("%Y-%m-%d")
    year_str = str(begin_day_obj.year)
    log_name = begin_day_obj.strftime("%Y%m%d")
    log_dir = TARGET_DIR + year_str
    log_name = log_dir + "\\" + log_name + ".log"
    #log_dir = os.path.dirname(log_name)
    if (not os.path.exists(log_dir)):
        os.makedirs(log_dir)

    with open(log_name,  'w', encoding='utf-8', newline='') as csvfile:
        logwriter = csv.writer(csvfile, delimiter='|')
        print("-------- Extract " + begin_day + " ----------")
        tsql = ('select FSPROGRAM, FDTIME, FSCATALOG, FSMESSAGE from TBTRACKINGLOG where FDTIME >= ? and FDTIME < ? order by FDTIME ')
        with cursor.execute(tsql, begin_day, end_day):
            row = cursor.fetchone()
            while row:
                fsprogram = row[0]
                fdtime = str(row[1])
                fscatalog = row[2]
                fsmessage = row[3]
                fsmessage = fsmessage.replace('\n', ' ').replace('\r', '').replace('|', ',')
                #log_msg = fsprogram + ":" + fdtime + ":" + fscatalog + ":" + fsmessage
                log_msg = [fsprogram, fdtime,  fscatalog,  fsmessage]
                #print(log_msg)
                logwriter.writerow(log_msg)

                row = cursor.fetchone()
    csvfile.close()
# end of extract_log

#begin_day_obj = date(2012, 1, 1)
begin_day_obj = date(2017, 7, 1)
end_day_obj = date(2017, 7, 31)

day = timedelta(days = 1)
count = 0
while begin_day_obj <= end_day_obj:
    next_day_obj = begin_day_obj + day
    extract_log(begin_day_obj, next_day_obj)
    begin_day_obj = begin_day_obj + day
    count += 1

cnxn.close()
    


"""
year_list = range(2012, 2018, 1)
month_list = range(1, 12, 1)
day_list = range(1, 31, 1)

for y in year_list:
    for m in month_list:
        for d in day_list:
            begin_date_str = str(y) + "-" + str(m) + "-" + str(d)
            end_date_str = str(y) + "-" + str(m) + "-" + str(d + 1)
            print(begin_date_str + "~" + end_date_str)
            #extract_log(begin_date_str, end_date_str)
            break
        break
    break
"""