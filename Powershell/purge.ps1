﻿    $LOGPath = "\\10.13.200.1\MasterCtrl\HD_FILE_MANAGER_LOG"
    $c = get-date -UFormat %Y%m%d
    #$prev = $c -1  #無法處裡跨月
    $prev = (get-date).AddDays(-1).ToString('yyyyMMdd')
    $deleteFile = "$LOGPath\D$c.log"
    $dest = "\\10.1.253.15\office\7-工程部\主控刪除清單"
    $purgeFile = "$dest\purge$c.txt"
    $LOG = "D:\WORKSPACE\LOG\PURGE_LOG.txt"
    $oldFile = "$dest\purge$prev.txt"
    #write-output $purgeFile
    $time = get-date

    #移除前一天的檔案
    if (test-path $oldfile) {
        Remove-Item -path $oldFile -Verbose | add-content $LOG
    }


    #exit if dest output is exist
    if (test-path $purgeFile) {
        Write-Output "$time : $purgeFile is exist" | add-content $LOG 
        exit
    }

    

    #exit if source file is not exist
    if (test-path $deleteFile) {
        Write-Output "$time : parsing $deleteFile" | add-content $LOG
        $lines = get-content $deleteFile
    } else {
        Write-Output "$time : $deleteFile is not exist" | add-content $LOG
        exit
    }

   
    #clear-content purge20160920.txt
    if (test-path $purgeFile) {
        clear-content $purgeFile
    }
    
    foreach ($l in $lines) {
        #write-output $l
        $video = $l.split(":")
        $videoID = $video[4].split(".")
        Write-Output $videoID[0] | add-Content $purgeFile -Verbose
    }


    #copy-item -path $purgeFile $dest