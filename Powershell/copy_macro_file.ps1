﻿#每隔半小時執行一次搬檔

function main {
    $macroFromQC = "\\10.13.200.6\Approved\MACRO\macro_approved_list.txt"
    $sourceDir = "\\10.13.200.6\Approved\"
    $destDir = "\\10.13.200.6\Approved\MACRO\"
    $LOG = "\\10.13.200.6\Approved\MACRO\LOG.txt"

    #get videoid list for QC approved, but not transcoded yet
    if (test-path $macroFromQC) {
        remove-item $macroFromQC -Verbose
    }
    BCP "select FSVIDEO_ID from MAM..VW_GET_CHECK_COMPLETE_LIST_MIX_MACRO" queryout $macroFromQC -c -S 10.13.210.33 -U "mam_admin" -P "ptsP@ssw0rd"


    #get file list in Approved\MACRO\
    $macroList = Get-ChildItem -path \\10.13.200.6\Approved\MACRO | %{$_.Name}

    #get file list in Approved
    #$res = Get-ChildItem -path \\10.13.200.6\Approved  | %{$_.FullName}
    $approvedList = Get-ChildItem -path \\10.13.200.6\Approved  | %{$_.Name}

    $lines = get-content -path $macroFromQC

    foreach ($videoID in $lines) {
        $f = $videoID.Substring(2,6) + ".mxf"
        #check if file exist in Approved\MACRO
        #$b = $macroList -match $f
        #write-output "result $b"
        $okF = $f + ".ok"
        if ($macroList -match "^$f$" -and $macroList -match "^$okF$") {
            continue
        } else {
            if ($approvedList -match $f) {
                $srcF = $sourceDir + $f
                $destF = $destDir + $f
                $okF = $destDir + $f + ".ok"
                $t = get-date
                write-output "$t : Begin copy $videoID from Approved to MACRO"
                try {           
                    copy-item -path $srcF -dest $destF -Verbose
                    write-output "$t : Begin copy $videoID from Approved to MACRO" | add-content $LOG

                    if ($?) {
                        new-item -path $okF -itemType "file"
                        $t = get-date
                        write-output "$t : End copy $videoID from Approved to MACRO" | add-content $LOG
                        write-output "$t : End copy $videoID from Approved to MACRO"
                    }
                } catch {
                    write-host "$t : $_.Exception.Message" -fore red

                }
                start-sleep -S 10  #for interrup
            } else {
                write-host "Cannot find $f in Approved..." -fore red
            }
        }
    }
}

while ($true) {
    main
    Start-Sleep -S 1800
}

