﻿#掃描 MarkUpload 的檔案，找出 00XXXX.mxf 檔案，比對  TBLOG_VIDEO_HD_MC 是否為 C，N，O

$HDUpload = '\\10.13.200.25\HDUpload'
$TARGET = "\\10.13.200.25\MarkUpload"
$bin = pwd
$rootDir = $ARGS[0]
$depth = 0
$DONOT_DELETE = "\\10.13.200.25\Review\NOT_DELETE.log"  #保留清單 C,N,O
$DELETE_LOG = "\\10.13.200.25\MarkUpload\PURGE.log"
$DO_DELETE = "\\10.13.200.25\Review\DELETE.log"  #O, T --> purge
$LOG = "\\isilon.pts.org.tw\MAM_PLAYOUT\MarkUpload\MOVE.txt"

<#
if (Test-Path -path $DELETE_LOG) {
    remove-item $DELETE_LOG
}
#>


#get files must keep 送播單狀態是標記上傳 待審核 審核通過 的檔案要保留
#X : 審核不通過的檔案,有可能只是修改送播單，還要重新上傳，先保留，日後再來刪除






function purge() {
    BCP "select FSVIDEO_ID from MAM..TBLOG_VIDEO_HD_MC where FCSTATUS not in ('C', 'N', 'O') and FSVIDEO_ID <> '' order by FDUPDATED_DATE desc" queryout $DONOT_DELETE -c -S 10.13.210.33 -U "mam_admin" -P "ptsP@ssw0rd"
    $keep_list = get-content -path $DONOT_DELETE
    $markUploadList = get-childitem -path $TARGET -Recurse | %{$_.FullName}
    foreach ($fPath in $markUploadList) {
        $ext = [System.IO.Path]::GetExtension($fPath)
        $base = [System.IO.Path]::GetFileNameWithoutExtension($fPath)
        if ($ext -eq ".MXF" -and ($base | select-string -pattern '.*00[0-9A-Z]{4}.*') ) {
            #write-output "Check $base, $fPath"
            #如果 00XXXX.mxf 不在保留清單就執行刪除
            $videoID = "00" + $base
            if ($keep_list.Contains($videoID) -eq $False) {
                $d = get-date
                #remove-item -path $fPath -verbose
                #write-output "$d : Remove $fPath" | Tee-Object -append $DELETE_LOG
                write-output "$d : Remove $fPath"           
            }
        }
    }
}


function purge_0822() {
    #purge O, T files
    BCP "select A.FSVIDEO_ID from MAM..TBLOG_VIDEO_HD_MC A left join MAM..TBLOG_VIDEO B on A.FSFILE_NO = B.FSFILE_NO where A.FCSTATUS = 'O' and B.FCFILE_STATUS = 'T'" queryout $DO_DELETE -c -S 10.13.210.33 -U "mam_admin" -P "ptsP@ssw0rd"
    $delete_list = get-content -path $DO_DELETE

    $markUploadList = get-childitem -path $TARGET -Recurse | %{$_.FullName}
    $count = 1
    $count2 = 1
    $totalSize = 0
    foreach ($fPath in $markUploadList) {
        if ((Get-Item $fPath) -is [System.IO.DirectoryInfo]) {
            continue
        }
        write-output "$count : Check $fPath"
        $ext = [System.IO.Path]::GetExtension($fPath)
        $base = [System.IO.Path]::GetFileNameWithoutExtension($fPath)    
        if ($ext -eq ".MXF" -and ($base -match '.*00[0-9A-Z]{4}.*') ) {
            #write-output "Check $base, $fPath"
            #如果 00XXXX.mxf 不在保留清單就執行刪除
            $videoID = "00" + $Matches[0]
            write-output "$count2 : Check $videoID"
            if ($delete_list.Contains($videoID) -eq $True) {
                $d = get-date
                #remove-item -path $fPath -verbose
                #write-output "$d : Remove $fPath" | Tee-Object -append $DELETE_LOG
                write-output "$d : Remove $fPath"           
            } else {
                $d = get-date
                #write-output "$d : Keep $fPath"  
            }
            $count2 = $count2 + 1
        } else {
            $size = (get-item -path $fPath).Length
            $totalSize = $totalSize + $size
        }
        $count = $count + 1
    }

    $totalSize = $totalSize / 1000000000
    write-output "Non MXF file : $totalSize GB"
}

function move_non_mxf() {
    $currentRoot = "\\10.13.200.25\MarkUpload"
    $isilon = "\\isilon.pts.org.tw\MAM_PLAYOUT\MarkUpload"
    $markUploadList = get-childitem -path $TARGET -Recurse | %{$_.FullName}
    $count = 1
    $count2 = 1
    $totalSize = 0
    foreach ($fPath in $markUploadList) {
        $obj = Get-Item $fPath
        if ($obj -is [System.IO.DirectoryInfo]) {
            continue
        }
        #write-output "$count : Check $fPath"
        $ext = $obj.Extension
        $base = $obj.BaseName  
        if ($ext -eq ".MXF" -and ($base | select-string -pattern '.*00[0-9A-Z]{4}.*')) {
            continue
        } else {
            $currentDir = $obj.DirectoryName
            $targetDir = $currentDir.Replace($currentRoot, $isilon)
            if ((test-path -path $targetDir) -eq $False) { 
                new-item -path $targetDir -itemtype directory
            }

            write-output "move $fPath to $targetDir" | Tee-Object -append $LOG
            move-item -path $fPath -Destination $targetDir
            #write-output "$fPath" | Tee-Object -append $LOG
            write-output "move $fPath to $targetDir"

            $size = (get-item -path $fPath).Length
            $totalSize = $totalSize + $size
        }
        $count = $count + 1
    }

    $totalSize = $totalSize / 1000000000
    write-output "Non MXF file : $totalSize GB"
}


#Traverse $cureentDir ex:\\10.13.200.25\MarkUpload
function purge4 ([string] $currentDir) {

    if (-not (test-path $currentDir)) {
        Write-Error "$currentDir is an invalid path"
        return $false
    }


    cd $currentDir
    $depth++
    $pwd = pwd
    #write-output "Current Dir ($depth) = $pwd"

    #$list = get-childitem -path $currentDir | %{$_.Name}
    $list = @(get-childitem -path ./)

    foreach ($obj in $list) {
        $target = "$currentDir/$obj"
        $isFile = test-path -path $target -pathType leaf
        if ($isFile) {
            #write-output "check $target ...."
            $orgSize = $obj.Length
            #write-output "$target size = $orgSize"
            $found = test-path -path "$HDUpload\$obj"
            if ($found) {
                $newSize = (get-item -path  "$HDUpload\$obj").Length
                #write-output "Found $obj in HDUpload, size = $newSize"
                if ($orgSize -eq $newSize) {
                   #write-output "Remove $target from MarkUpload"
                   write-output "$target" | add-content $bin/purge_mark_upload-0928.txt
                   #write-output "$target"
                }
            }
        
        } else {
            #write-output "   Step into $target ...."
            purge $target
        }
    }
}


function purge1 ([string] $currentDir) {
    write-output "Step into $currentDir ...."
    cd $currentDir
    $pwd = pwd
    write-output "Current = $pwd"
    $list = @(get-childitem -path ./)
}



function purge_test ([string] $currentDir) {

    if (-not (test-path $currentDir)) {
        Write-Error "$currentDir is an invalid path"
        return $false
    }


    cd $currentDir
    $depth++
    $pwd = pwd
    write-output "Current Dir ($depth) = $pwd"

    #$list = get-childitem -path $currentDir | %{$_.Name}
    $list = @(get-childitem -path ./)

    foreach ($obj in $list) {
        $target = "$currentDir/$obj"
        $isFile = test-path -path $target -pathType leaf
        write-output "$target Is file : $isFile"
        if ($isFile) {
            write-output "check $target ...."
        } else {
            write-output "   Step into $target ...."
            purge $target
        }
    }
}


#purge_0822
move_non_mxf

