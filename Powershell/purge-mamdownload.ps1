﻿

#根據建立日期刪除 MAMDownload 7天前調用的檔案
function purge {
    $TARGET_DIR = "\\10.13.200.25\MAMDownload"
    $fileList = get-childitem -path $TARGET_DIR -File -Recurse | sort-object CreationTime | where {$_.FullName -notmatch "hsing1"} | %{$_.fullName}
    $currentDate = get-date
    foreach ($f in $fileList) {
        $createdDate = (get-item "$f").CreationTime
        if ($createdDate -le $currentDate.AddDays(-7)) {
            #write-output "$f : $createdDate, remove"
            remove-item -path $f -verbose
        }

    }
}

purge
